$(function(){
    $(document).on("click", ".btn-sikayet", function () {
        $(".modal-content #button-modal").attr("onclick", "window.location.href='" + $(this).data('id') + "'");
    });

    $(document).on("click", ".btn-kredi", function () {
        $("#content-kredi #content-kredi-aciklama").html($(this).data('content'));
    });

    $(document).on("click", ".btn-kredi-kullanim", function () {
        $("#content-kredi-kullanim #content-kredi-kullanim-aciklama").html($(this).data('content'));
        $("#content-kredi-kullanim .btn").val($(this).data('submit-value'))
    });

    $(document).on("click", "#teklif_button", function () {
        $("#teklif_buton_div").hide(1000);
        $("#teklif_yaz_div").show(1000);
    });

    $('#teklif_gonder').on('click', function (e) {
        if (isContentEmpty($('.richText-editor').html()) == false) {
            alert("L�tfen teklif a��klaman�z� en az 100 karakter yaz�n�z.");
            return false;
        }
    });

    $(".btnstat-modal").click(function () {
        $("#content-dosya .modal-title").html($(this).attr("title"));
        $("#content-dosya").modal();
        load_page_s($(this).attr('href'));
    });

    $(".select2").select2({
        minimumResultsForSearch: Infinity
    });

    $("#btnfile").click(function () {
        $("#uplodfile").click();
    });

    $("#uplodfile").change(function () {
        $("#uploadform").submit();
    });
});

function load_page_s(url) {
    $('#content-dosya #body').load(url, function () { });
}

function isContentEmpty(text) {
    text = text.replaceAll("&nbsp;", "").
        replaceAll("<div>", "").
        replaceAll("</div>", "").
        replaceAll("<br>", "").
        replaceAll("<p>", "").
        replaceAll("</p>", "").trim();

    return (text.length < 100) ? false : true;
}
﻿using System.Web;
using System.Web.Optimization;

namespace AvukatliknetOnline
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/css").Include(
                                  "~/Content/fonts/fonts/font-awesome.min.css",
                                  "~/Content/plugins/toggle-sidebar/sidemenu.css",
                                  "~/Content/plugins/bootstrap/css/bootstrap.min.css",
                                  "~/Content/css/style.css",
                                  "~/Content/plugins/datatable/dataTables.bootstrap4.min.css",
                                  "~/Content/plugins/datatable/jquery.dataTables.min.css",
                                  "~/Content/css/admin-custom.css",
                                  "~/Content/plugins/charts-c3/c3-chart.css",
                                  "~/Content/plugins/morris/morris.css",
                                  "~/Content/plugins/scroll-bar/jquery.mCustomScrollbar.css",
                                  "~/Content/plugins/select2/select2.min.css",
                                  "~/Content/plugins/iconfonts/plugin.css",
                                  "~/Content/css/icons.css",
                                  "~/Content/plugins/wysiwyag/richtext.css",
                                  "~/Content/switcher/css/switcher.css"));


            bundles.Add(new ScriptBundle("~/Content/js").Include(
                        "~/Content/plugins/bootstrap/js/popper.min.js",
                        "~/Content/plugins/bootstrap/js/bootstrap.min.js",
                        "~/Content/js/vendors/jquery.sparkline.min.js",
                        "~/Content/js/vendors/selectize.min.js",
                        "~/Content/plugins/datatable/jquery.dataTables.min.js",
                        "~/Content/plugins/datatable/dataTables.bootstrap4.min.js",
                        "~/Content//js/datatable.js",
                        "~/Content/js/vendors/jquery.tablesorter.min.js",
                        "~/Content/js/vendors/circle-progress.min.js",
                        "~/Content/plugins/rating/jquery.rating-stars.js",
                        "~/Content/plugins/toggle-sidebar/sidemenu.js",
                        "~/Content/plugins/chart/Chart.bundle.js",
                        "~/Content/plugins/chart/utils.js",
                        "~/Content/plugins/morris/raphael-min.js",
                        "~/Content/assets/plugins/morris/morris.js",
                        "~/Content/plugins/select2/select2.full.min.js",
                        "~/Content/plugins/input-mask/jquery.mask.min.js",
                        "~/Content/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js",
                        "~/Content/plugins/counters/counterup.min.js",
                        "~/Content/plugins/counters/waypoints.min.js",
                        "~/Content/plugins/wysiwyag/jquery.richtext.js",
                        "~/Content/js/admin-custom.js",
                        "~/Content/js/formeditor.js",
                        "~/Content/js/scripts.js"));
        }
    }
}

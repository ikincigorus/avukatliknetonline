namespace AvukatliknetOnline.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class T_AVUKAT_BRANS
    {
        public long id { get; set; }

        public long? avukat_id { get; set; }

        public int? brans_id { get; set; }

        public virtual T_AVUKAT T_AVUKAT { get; set; }

        public virtual T_BRANS T_BRANS { get; set; }
    }
}

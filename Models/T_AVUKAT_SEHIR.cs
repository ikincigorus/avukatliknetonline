namespace AvukatliknetOnline.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class T_AVUKAT_SEHIR
    {
        public long id { get; set; }

        public long? avukat_id { get; set; }

        public int? il_id { get; set; }

        public virtual T_AVUKAT T_AVUKAT { get; set; }

        public virtual T_SEHIR T_SEHIR { get; set; }
    }
}

namespace AvukatliknetOnline.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class T_BRANS
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public T_BRANS()
        {
            T_AVUKAT_BRANS = new HashSet<T_AVUKAT_BRANS>();
            T_SIKAYET = new HashSet<T_SIKAYET>();
        }

        public int id { get; set; }

        [StringLength(50)]
        public string brans_adi { get; set; }

        [StringLength(300)]
        public string aciklama { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_AVUKAT_BRANS> T_AVUKAT_BRANS { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_SIKAYET> T_SIKAYET { get; set; }
    }
}

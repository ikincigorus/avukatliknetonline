namespace AvukatliknetOnline.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class T_SLIDER_AVUKAT
    {
        public long id { get; set; }

        public long? avukat_id { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? baslama_tarihi { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? bitis_tarihi { get; set; }

        public long? sira_no { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? kayit_tarihi { get; set; }

        public virtual T_AVUKAT T_AVUKAT { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AvukatliknetOnline.Models
{
    public static class _users
    {
        public static long CurrId
        {
            get
            {
                long CurrId = 0;

                if (HttpContext.Current.Request.IsAuthenticated)
                {
                    CurrId = Convert.ToInt64(HttpContext.Current.User.Identity.Name.Split('~')[0]);
                }

                return CurrId;
            }
        }

        public static string CurrAvukatKod
        {
            get
            {
                string CurrAvukatKod = "";

                if (HttpContext.Current.Request.IsAuthenticated)
                {
                    CurrAvukatKod = HttpContext.Current.User.Identity.Name.Split('~')[1];
                }

                return CurrAvukatKod;
            }
        }

        public static string CurrAdiSoyadi
        {
            get
            {
                string CurrAdiSoyadi = "";

                if (HttpContext.Current.Request.IsAuthenticated)
                {
                    CurrAdiSoyadi = HttpContext.Current.User.Identity.Name.Split('~')[2];
                }

                return CurrAdiSoyadi;
            }
        }

        public static string CurrEposta
        {
            get
            {
                string CurrEposta = "";

                if (HttpContext.Current.Request.IsAuthenticated)
                {
                    CurrEposta = HttpContext.Current.User.Identity.Name.Split('~')[3];
                }

                return CurrEposta;
            }
        }

        public static int CurrYonetici
        {
            get
            {
                int CurrYonetici = 0;

                if (HttpContext.Current.Request.IsAuthenticated)
                {
                    CurrYonetici = Convert.ToInt16(HttpContext.Current.User.Identity.Name.Split('~')[4]);
                }

                return CurrYonetici;
            }
        }

        public static string CurrResim
        {
            get
            {
                string CurrResim = "";

                if (HttpContext.Current.Request.IsAuthenticated)
                {
                    CurrResim = HttpContext.Current.User.Identity.Name.Split('~')[5];
                }

                return CurrResim;
            }
        }
    }
}
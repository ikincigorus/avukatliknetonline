using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace AvukatliknetOnline.Models
{
    public partial class EntityModel : DbContext
    {
        public EntityModel()
            : base("name=EntityModel")
        {
        }

        public virtual DbSet<T_AVUKAT> T_AVUKAT { get; set; }
        public virtual DbSet<T_AVUKAT_BRANS> T_AVUKAT_BRANS { get; set; }
        public virtual DbSet<T_AVUKAT_DOSYA_ISLEM> T_AVUKAT_DOSYA_ISLEM { get; set; }
        public virtual DbSet<T_AVUKAT_KREDI_LOG> T_AVUKAT_KREDI_LOG { get; set; }
        public virtual DbSet<T_AVUKAT_SEHIR> T_AVUKAT_SEHIR { get; set; }
        public virtual DbSet<T_BRANS> T_BRANS { get; set; }
        public virtual DbSet<T_KREDI_PLAN> T_KREDI_PLAN { get; set; }
        public virtual DbSet<T_PARAMS> T_PARAMS { get; set; }
        public virtual DbSet<T_SEHIR> T_SEHIR { get; set; }
        public virtual DbSet<T_SIKAYET> T_SIKAYET { get; set; }
        public virtual DbSet<T_SIKAYET_AVUKAT> T_SIKAYET_AVUKAT { get; set; }
        public virtual DbSet<T_SLIDER_AVUKAT> T_SLIDER_AVUKAT { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<T_AVUKAT>()
                .HasMany(e => e.T_AVUKAT_BRANS)
                .WithOptional(e => e.T_AVUKAT)
                .HasForeignKey(e => e.avukat_id);

            modelBuilder.Entity<T_AVUKAT>()
                .HasMany(e => e.T_AVUKAT_DOSYA_ISLEM)
                .WithOptional(e => e.T_AVUKAT)
                .HasForeignKey(e => e.avukat_id);

            modelBuilder.Entity<T_AVUKAT>()
                .HasMany(e => e.T_AVUKAT_SEHIR)
                .WithOptional(e => e.T_AVUKAT)
                .HasForeignKey(e => e.avukat_id);

            modelBuilder.Entity<T_AVUKAT>()
                .HasMany(e => e.T_SIKAYET_AVUKAT)
                .WithOptional(e => e.T_AVUKAT)
                .HasForeignKey(e => e.avukat_id);

            modelBuilder.Entity<T_AVUKAT>()
                .HasMany(e => e.T_SLIDER_AVUKAT)
                .WithOptional(e => e.T_AVUKAT)
                .HasForeignKey(e => e.avukat_id);

            modelBuilder.Entity<T_AVUKAT>()
                .HasMany(e => e.T_AVUKAT_KREDI_LOG)
                .WithOptional(e => e.T_AVUKAT)
                .HasForeignKey(e => e.avukat_id);

            modelBuilder.Entity<T_BRANS>()
                .HasMany(e => e.T_AVUKAT_BRANS)
                .WithOptional(e => e.T_BRANS)
                .HasForeignKey(e => e.brans_id);

            modelBuilder.Entity<T_BRANS>()
                .HasMany(e => e.T_SIKAYET)
                .WithOptional(e => e.T_BRANS)
                .HasForeignKey(e => e.brans_id);

            modelBuilder.Entity<T_SEHIR>()
                .HasMany(e => e.T_AVUKAT)
                .WithOptional(e => e.T_SEHIR)
                .HasForeignKey(e => e.il_id);

            modelBuilder.Entity<T_SEHIR>()
                .HasMany(e => e.T_AVUKAT_SEHIR)
                .WithOptional(e => e.T_SEHIR)
                .HasForeignKey(e => e.il_id);

            modelBuilder.Entity<T_SEHIR>()
                .HasMany(e => e.T_SIKAYET)
                .WithOptional(e => e.T_SEHIR)
                .HasForeignKey(e => e.il_id);

            modelBuilder.Entity<T_SIKAYET>()
                .HasMany(e => e.T_AVUKAT_DOSYA_ISLEM)
                .WithOptional(e => e.T_SIKAYET)
                .HasForeignKey(e => e.sikayet_id);

            modelBuilder.Entity<T_SIKAYET>()
                .HasMany(e => e.T_SIKAYET_AVUKAT)
                .WithOptional(e => e.T_SIKAYET)
                .HasForeignKey(e => e.sikayet_id);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace AvukatliknetOnline.Models
{
    public static class _common
    {
        private static EntityModel db = new EntityModel();

        [ErrorLog]
        public static string KacinciGun(DateTime tarih)
        {
            return (DateTime.Today.Day - tarih.Day).ToString();
        }

        [ErrorLog]
        public static object ParamKey(string grup, string key)
        {
            object value;

            if (key == "")
                value = db.T_PARAMS.Where(t => t.grup == grup).ToList();
            else
                value = db.T_PARAMS.Where(t => t.grup == grup && t.keys == key).FirstOrDefault().value;

            return value;
        }

        public static string PnrGenerate()
        {
            string pnr = String.Format("{0:d6}", (DateTime.Now.Ticks / 10) % 1000000000);
            return CreatePnr(2) + pnr;
        }

        private static string CreatePnr(int length)
        {
            const string valid = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]);
            }
            return res.ToString();
        }

        public static string GetHashString(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(inputString))
                sb.Append(b.ToString("X2"));

            return sb.ToString();
        }

        public static string MaskName(string inputString)
        {
            string[] arr = inputString.Split(' ');
            string retVal = "";
            for (int i = 0; i < arr.Length; i++)
            {
                for (int j = 0; j < arr[i].Length; j++)
                {
                    if (j == 0)
                        retVal += arr[i].ToCharArray()[0];
                    else
                        retVal += '*';
                }
                if (i < arr.Length - 1)
                    retVal += ' ';
            }


            return retVal;
        }

        private static byte[] GetHash(string inputString)
        {
            using (HashAlgorithm algorithm = SHA256.Create())
                return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        public static void SendMail(string to, string subject, string body)
        {
            _mailing.MailSender(to, subject, body, true);
        }

        public static void SystemSendMail(string subject, string body)
        {
            _mailing.MailSender("avukatliknet2@gmail.com", subject, body, true);
        }

        public static string CreatePassword(int length)
        {
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]);
            }
            return res.ToString();
        }

        public static bool CheckForSQLInjection(string userInput)
        {
            bool isSQLInjection = false;
            string[] sqlCheckList = { "--", ";--", ";", "/*", "*/", "@@", "@", "char", "nchar", "varchar", "nvarchar", "alter", "begin", "cast", "create", "cursor", "declare", "delete", "drop", "end", "exec", "execute", "fetch", "insert", "kill", "select", "sys", "sysobjects", "syscolumns", "table", "update" };
            string CheckString = userInput.Replace("'", "''");
            for (int i = 0; i <= sqlCheckList.Length - 1; i++)
            {
                if ((CheckString.IndexOf(sqlCheckList[i], StringComparison.OrdinalIgnoreCase) >= 0))
                    isSQLInjection = true;
            }
            return isSQLInjection;
        }

        public static string EnCryptIt(string toEnrypt)
        {
            if (!string.IsNullOrEmpty(toEnrypt))
            {
                try
                {
                    byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(TurkishEnCode(toEnrypt));
                    byte[] rgbKey = System.Text.ASCIIEncoding.ASCII.GetBytes("01010101"); //buradaki string parametreler sizin crypto keyini’niz.
                    byte[] rgbIV = System.Text.ASCIIEncoding.ASCII.GetBytes("02020202"); //buradaki string parametreler sizin crypto value’nuz.
                    MemoryStream memoryStream = new MemoryStream(1024);
                    DESCryptoServiceProvider desCryptoServiceProvider = new DESCryptoServiceProvider();
                    CryptoStream cryptoStream = new CryptoStream(memoryStream, desCryptoServiceProvider.CreateEncryptor(rgbKey, rgbIV), CryptoStreamMode.Write);
                    cryptoStream.Write(data, 0, data.Length);
                    cryptoStream.FlushFinalBlock();
                    byte[] result = new byte[(int)memoryStream.Position];
                    memoryStream.Position = 0;
                    memoryStream.Read(result, 0, result.Length);
                    cryptoStream.Close();
                    return System.Convert.ToBase64String(result);
                }
                catch (CryptographicException ex)
                {
                    throw ex;
                }
            }
            return "";
        }

        public static string DeCryptIt(string toDecrypt)
        {
            string decrypted = string.Empty;
            if (!string.IsNullOrEmpty(toDecrypt))
            {
                try
                {
                    byte[] data = System.Convert.FromBase64String(toDecrypt);
                    byte[] rgbKey = System.Text.ASCIIEncoding.ASCII.GetBytes("01010101");
                    byte[] rgbIV = System.Text.ASCIIEncoding.ASCII.GetBytes("02020202");
                    MemoryStream memoryStream = new MemoryStream(data.Length);
                    DESCryptoServiceProvider desCryptoServiceProvider = new DESCryptoServiceProvider();
                    CryptoStream cryptoStream = new CryptoStream(memoryStream, desCryptoServiceProvider.CreateDecryptor(rgbKey, rgbIV), CryptoStreamMode.Read);
                    memoryStream.Write(data, 0, data.Length);
                    memoryStream.Position = 0;
                    decrypted = new StreamReader(cryptoStream).ReadToEnd();
                    cryptoStream.Close();
                }
                catch (CryptographicException ex)
                {
                    throw ex;
                }
            }
            return TurkishDeCode(decrypted);
        }

        public static string TurkishEnCode(string Text) // Türkçe karakteri İngilizce karaktere çevir
        {
            return Text.Replace("ı", "z001").Replace("İ", "z002").Replace("â", "z003").Replace("ç", "z004").Replace("Ç", "z005").
            Replace("ğ", "z006").Replace("Ğ", "z007").Replace("ö", "z008").Replace("Ö", "z009").
            Replace("ş", "z010").Replace("Ş", "z011").Replace("ü", "z012").Replace("Ü", "z013");
        }

        public static string TurkishDeCode(string Text) // Türkçe karakteri İngilizce karaktere çevir
        {
            return Text.Replace("z001", "ı").Replace("z002", "İ").Replace("z003", "â").Replace("z004", "ç").Replace("z005", "Ç").
            Replace("z006", "ğ").Replace("z007", "Ğ").Replace("z008", "ö").Replace("z009", "Ö").
            Replace("z010", "ş").Replace("z011", "Ş").Replace("z012", "ü").Replace("z013", "Ü");
        }

        public static DataTable ToDataTable<T>(this IList<T> data)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }

        public static DataTable ParamaterList(List<T_PARAMS> Collection)
        {
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[3] { new DataColumn("A"), new DataColumn("B"), new DataColumn("C") });
            foreach (var item in Collection)
            {
                dt.Rows.Add(item.grup, item.keys, item.value);
            }
            return dt;
        }

        public static string OnlyLoginString(string userInput)
        {
            return Regex.Replace(userInput, "[^A-Za-z0-9@.]", "");
        }

        public static string OnlyCaptchaNumber(string userInput)
        {
            return Regex.Replace(userInput, "[^0-9]", "");
        }

        public static string GetGuid()
        {
            return Guid.NewGuid().ToString().Replace("-", "").ToUpper();
        }

        public static string GetUrl(int? dashboard, string returnUrl)
        {
            if (returnUrl == null)
            {
                switch (dashboard)
                {
                    case 0:
                        return "/Avukat/Dashboard";
                    case 10:
                        return "/Yonetim/Dashboard";
                    default:
                        return "/Login";
                }
            }
            else
            {
                return returnUrl;
            }
        }

        public static void ResizeStream(int imageSize, Stream filePath, string outputPath, string fileName)
        {
            var image = Image.FromStream(filePath);

            int thumbnailSize = imageSize;
            int newWidth, newHeight;

            if (image.Width > image.Height)
            {
                newWidth = thumbnailSize;
                newHeight = image.Height * thumbnailSize / image.Width;
            }
            else
            {
                newWidth = image.Width * thumbnailSize / image.Height;
                newHeight = thumbnailSize;
            }

            var thumbnailBitmap = new Bitmap(newWidth, newHeight);

            var thumbnailGraph = Graphics.FromImage(thumbnailBitmap);
            thumbnailGraph.CompositingQuality = CompositingQuality.HighQuality;
            thumbnailGraph.SmoothingMode = SmoothingMode.HighQuality;
            thumbnailGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;

            var imageRectangle = new Rectangle(0, 0, newWidth, newHeight);
            thumbnailGraph.DrawImage(image, imageRectangle);

            thumbnailBitmap.Save(outputPath + fileName, image.RawFormat);
            thumbnailGraph.Dispose();
            thumbnailBitmap.Dispose();
            image.Dispose();

            /*Image orgImg = Image.FromFile(outputPath + fileName);
            Rectangle CropArea = new Rectangle(1, 150, newWidth, newHeight);
            Bitmap bitMap = new Bitmap(CropArea.Width, CropArea.Height);
            using (Graphics g = Graphics.FromImage(bitMap))
            {
                g.DrawImage(orgImg, new Rectangle(0, 0, bitMap.Width, bitMap.Height), CropArea, GraphicsUnit.Pixel);
            }
            bitMap.Save(outputPath + "Thumb\\" + fileName);*/
        }

    }
}
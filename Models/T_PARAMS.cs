namespace AvukatliknetOnline.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class T_PARAMS
    {
        public int id { get; set; }

        [StringLength(50)]
        public string grup { get; set; }

        [StringLength(50)]
        public string keys { get; set; }

        [StringLength(50)]
        public string value { get; set; }

        public int? sira_no { get; set; }
    }
}

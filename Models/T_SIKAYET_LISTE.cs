namespace AvukatliknetOnline.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class T_SIKAYET_LISTE
    {
        
        public long id { get; set; }

        [StringLength(20)]
        public string dosya_kod { get; set; }

        [StringLength(50)]
        public string adi_soyadi { get; set; }

        public string il_adi { get; set; }

        public string brans_adi { get; set; }

        public int teklif_sayisi { get; set; }

        public int teklif_okundumu { get; set; }

        public int teklif_verdimi { get; set; }
        
        [Column(TypeName = "smalldatetime")]
        public DateTime sikayet_tarihi { get; set; }
    }
}

﻿using AvukatliknetOnline.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace AvukatliknetOnline.Controllers
{
    public class LoginController : Controller
    {
        EntityModel db = new EntityModel();
        _site site = new _site();

        [ErrorLog]
        [AllowAnonymous]
        public ActionResult Index()
        {
            CreateCaptchaCode();
            return View(site);
        }

        [ErrorLog]
        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(string e_posta_adresi, string sifre, string captcha, string returnUrl)
        {
            bool isTest = true;

            if (Session["CaptchaCode"].ToString() == _common.OnlyCaptchaNumber(captcha) || isTest)
            {
                e_posta_adresi = _common.OnlyLoginString(e_posta_adresi.ToLower());
                site.M_AVUKAT_GIRIS = db.T_AVUKAT.Where(u => u.e_posta_adresi.ToLower() == e_posta_adresi && u.durumu == 0).FirstOrDefault();

                if (site.M_AVUKAT_GIRIS != null)
                {
                    sifre = _common.GetHashString(sifre);
                    site.M_AVUKAT_GIRIS = db.T_AVUKAT.Where(u => u.e_posta_adresi.ToLower() == e_posta_adresi /*&& u.sifre == sifre*/ && u.durumu == 0).FirstOrDefault();
                    if (site.M_AVUKAT_GIRIS != null || isTest)
                    {
                        FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                        1,
                        site.M_AVUKAT_GIRIS.id + "~" + site.M_AVUKAT_GIRIS.avukat_code + "~" + site.M_AVUKAT_GIRIS.adi_soyadi + "~" + site.M_AVUKAT_GIRIS.e_posta_adresi + "~" + site.M_AVUKAT_GIRIS.yonetici + "~" + site.M_AVUKAT_GIRIS.avukat_resim,
                        DateTime.Now,
                        DateTime.Now.AddHours(4),
                        true,
                        site.M_AVUKAT_GIRIS.yonetici.ToString(),
                        FormsAuthentication.FormsCookiePath);

                        string hash = FormsAuthentication.Encrypt(ticket);
                        HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, hash);

                        Response.Cookies.Add(cookie);

                        return Redirect(_common.GetUrl(site.M_AVUKAT_GIRIS.yonetici, returnUrl));
                    }
                    
                    ViewBag.message = "<div class=\"alert alert-danger\" role=\"alert\"><div class=\"iq-alert-icon\"><i class=\"ri-information-line\"></i></div><div class=\"iq-alert-text\">Şifreniz hatalıdır.</div></div>";

                    CreateCaptchaCode();

                    return View(site);
                }

                ViewBag.message = "<div class=\"alert alert-danger\" role=\"alert\"><div class=\"iq-alert-icon\"><i class=\"ri-information-line\"></i></div><div class=\"iq-alert-text\">Bilgileriniz hatalı !</div></div>";
            }
            else
            {
                ViewBag.message = "<div class=\"alert alert-danger\" role=\"alert\"><div class=\"iq-alert-icon\"><i class=\"ri-information-line\"></i></div><div class=\"iq-alert-text\">Güvenlik kodu hatalı !</div></div>";
            }

            CreateCaptchaCode();

            return View(site);
        }

        [ErrorLog]
        [AllowAnonymous]
        public ActionResult Sifremi_Unuttum()
        {
            CreateCaptchaCode();
            return View(site);
        }

        [ErrorLog]
        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Sifremi_Unuttum(string e_posta_adresi, string captcha)
        {
            if (Session["CaptchaCode"].ToString() == captcha)
            {
                T_AVUKAT AvukatGiris = db.T_AVUKAT.Where(u => u.e_posta_adresi.ToLower() == e_posta_adresi.ToLower() && u.durumu == 1).FirstOrDefault();
                if (AvukatGiris == null)
                {
                    ViewBag.message = "<div class=\"alert alert-danger\" role=\"alert\"><div class=\"iq-alert-icon\"><i class=\"ri-information-line\"></i></div><div class=\"iq-alert-text\">E-mail adresiniz sistemde bulunamadı!</div></div>";
                }
                else
                {
                    string password = _common.CreatePassword(6);
                    string passwordhash = _common.GetHashString(password);
                    AvukatGiris.sifre = passwordhash;
                    db.SaveChanges();
                    _common.SendMail(AvukatGiris.e_posta_adresi, "Avukatlik.net sistemi kullanıcı şifreniz", "Merhaba " + AvukatGiris.adi_soyadi + "<br><br>İkinci göz sistemi için<br><br>Kullanıcı Adınız : " + AvukatGiris.e_posta_adresi + "<br>Şifreniz : " + password);
                    ViewBag.message = "<div class=\"alert alert-success\" role=\"alert\"><div class=\"iq-alert-icon\"><i class=\"ri-information-line\"></i></div><div class=\"iq-alert-text\">Şifreniz E-mail adresinize gönderildi.</div></div>";
                }
            }
            else
            {
                ViewBag.message = "<div class=\"alert alert-danger\" role=\"alert\"><div class=\"iq-alert-icon\"><i class=\"ri-information-line\"></i></div><div class=\"iq-alert-text\">Güvenli kodu hatalı !</div></div>";
            }

            CreateCaptchaCode();

            return View(site);
        }


        [ErrorLog]
        [Authorize]
        public ActionResult Oturumu_Sonlandir()
        {
            FormsAuthentication.SignOut();
            return Redirect("/Login");
        }

        [ErrorLog]
        [AllowAnonymous]
        public FileContentResult GetCaptchaCode()
        {
            return GetCaptcha2();
        }

        [ErrorLog]
        [AllowAnonymous]
        private FileContentResult GetCaptcha2()
        {
            _captchaImage ci = new _captchaImage(Session["CaptchaCode"].ToString(), 151, 39, "Arial");
            MemoryStream ms = new MemoryStream();
            ci.Image.Save(ms, ImageFormat.Jpeg);
            ci.Dispose();
            byte[] imageBytes = ms.ToArray();
            FileContentResult img = File(ms.ToArray(), "image/gif");
            //string base64String = Convert.ToBase64String(imageBytes);
            return File(img.FileContents, img.ContentType);
        }

        [ErrorLog]
        [AllowAnonymous]
        private string GetCaptcha()
        {
            _captchaImage ci = new _captchaImage(Session["CaptchaCode"].ToString(), 151, 39, "Arial");
            MemoryStream ms = new MemoryStream();
            ci.Image.Save(ms, ImageFormat.Jpeg);
            ci.Dispose();
            byte[] imageBytes = ms.ToArray();
            //FileContentResult img = File(ms.ToArray(), "image/gif");
            string base64String = Convert.ToBase64String(imageBytes);
            return base64String; //File(img.FileContents, img.ContentType);
        }

        [ErrorLog]
        [AllowAnonymous]
        public FileContentResult CreateCaptchaCode()
        {
            return CreateCaptcha(GenerateRandomCode());
        }

        [ErrorLog]
        [AllowAnonymous]
        private FileContentResult CreateCaptcha(string generate)
        {
            Session["CaptchaCode"] = generate;
            _captchaImage ci = new _captchaImage(Session["CaptchaCode"].ToString(), 200, 41, "Arial");
            MemoryStream ms = new MemoryStream();
            ci.Image.Save(ms, ImageFormat.Jpeg);
            ci.Dispose();
            FileContentResult img = File(ms.ToArray(), "image/gif");
            return File(img.FileContents, img.ContentType);
        }

        [ErrorLog]
        private string GenerateRandomCode()
        {
            Random random = new Random();
            string s = "";
            for (int i = 0; i < 6; i++)
                s = String.Concat(s, random.Next(10).ToString());
            return s;
        }
    }
}
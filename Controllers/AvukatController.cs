﻿using AvukatliknetOnline.Models;
using Microsoft.Security.Application;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.SqlServer;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AvukatliknetOnline.Controllers
{
    public class AvukatController : Controller
    {
        // GET: Avukat
        EntityModel db = new EntityModel();
        _site site = new _site();
        public static readonly string link = ConfigurationManager.AppSettings["urllink"];
        public static readonly string onlinelink = ConfigurationManager.AppSettings["onlinelink"];
        public static String odendiMi = "";

        [ErrorLog]
        [Authorize(Roles = "0")]
        public ActionResult Dashboard()
        {
            site.M_AVUKAT = db.T_AVUKAT.Where(t => t.id == site.CurrId).FirstOrDefault();
            site.M_SIKAYET_LISTES = (from a in db.T_AVUKAT_BRANS
                                     from b in db.T_AVUKAT_SEHIR
                                     join c in db.T_SIKAYET on a.brans_id equals c.brans_id
                                     join d in db.T_SIKAYET on b.il_id equals d.il_id
                                     where c.id == d.id
                                     && c.durumu == 0
                                     && site.CurrId == a.avukat_id
                                     && site.CurrId == b.avukat_id
                                     && d.T_SIKAYET_AVUKAT.Where(t => t.avukat_id == site.CurrId).Count() == 0
                                     select new T_SIKAYET_LISTE
                                     {
                                         id = d.id,
                                         dosya_kod = d.dosya_kod,
                                         adi_soyadi = d.adi_soyadi,
                                         il_adi = d.T_SEHIR.il_adi,
                                         brans_adi = d.T_BRANS.brans_adi,
                                         sikayet_tarihi = d.sikayet_tarihi,
                                         teklif_sayisi = d.T_SIKAYET_AVUKAT.Count(),
                                         teklif_okundumu = d.T_AVUKAT_DOSYA_ISLEM.Where(t => t.dosya_islem == 1).Count(),
                                         teklif_verdimi = d.T_SIKAYET_AVUKAT.Where(t => t.avukat_id == site.CurrId).Count()
                                     }).OrderByDescending(t => t.sikayet_tarihi).ToList();

            site.KrediParams = Convert.ToString(_common.ParamKey("avukat_sikayet_okuma", "avukat_sikayet_okuma"));

            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "0")]
        public ActionResult Gelen_Dosyalar()
        {
            T_AVUKAT avukat = db.T_AVUKAT.Where(t => t.id == site.CurrId).FirstOrDefault();
            site.M_SIKAYET_LISTES = (from a in db.T_AVUKAT_BRANS
                                     from b in db.T_AVUKAT_SEHIR
                                     join c in db.T_SIKAYET on a.brans_id equals c.brans_id
                                     join d in db.T_SIKAYET on b.il_id equals d.il_id
                                     where c.id == d.id
                                     && c.durumu == 0
                                     && avukat.id == a.avukat_id
                                     && avukat.id == b.avukat_id
                                     && c.T_SIKAYET_AVUKAT.Where(t => t.avukat_id == avukat.id).Count() == 0
                                     select new T_SIKAYET_LISTE
                                     {
                                         id = d.id,
                                         dosya_kod = d.dosya_kod,
                                         adi_soyadi = d.adi_soyadi,
                                         il_adi = d.T_SEHIR.il_adi,
                                         brans_adi = d.T_BRANS.brans_adi,
                                         sikayet_tarihi = d.sikayet_tarihi,
                                         teklif_sayisi = d.T_SIKAYET_AVUKAT.Count(),
                                         teklif_okundumu = d.T_AVUKAT_DOSYA_ISLEM.Where(t => t.dosya_islem == 1).Count()
                                     }).OrderByDescending(t => t.sikayet_tarihi).ToList();

            site.KrediParams = Convert.ToString(_common.ParamKey("avukat_sikayet_okuma", "avukat_sikayet_okuma"));

            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "0")]
        public ActionResult Teklif_Verilen_Dosyalar()
        {
            T_AVUKAT avukat = db.T_AVUKAT.Where(t => t.id == site.CurrId).FirstOrDefault();
            site.M_SIKAYET_LISTES = (from a in db.T_AVUKAT_BRANS
                                     from b in db.T_AVUKAT_SEHIR
                                     join c in db.T_SIKAYET on a.brans_id equals c.brans_id
                                     join d in db.T_SIKAYET on b.il_id equals d.il_id
                                     where c.id == d.id
                                     && c.durumu == 0
                                     && avukat.id == a.avukat_id
                                     && avukat.id == b.avukat_id
                                     && d.T_SIKAYET_AVUKAT.Where(t => t.avukat_id == avukat.id).Count() > 0
                                     select new T_SIKAYET_LISTE
                                     {
                                         id = d.id,
                                         dosya_kod = d.T_SIKAYET_AVUKAT.FirstOrDefault().sikayet_kodu,
                                         adi_soyadi = d.adi_soyadi,
                                         il_adi = d.T_SEHIR.il_adi,
                                         brans_adi = d.T_BRANS.brans_adi,
                                         sikayet_tarihi = d.sikayet_tarihi,
                                         teklif_sayisi = d.T_SIKAYET_AVUKAT.Count()
                                     }).OrderByDescending(t => t.sikayet_tarihi).ToList();

            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "0")]
        public ActionResult Teklif_Ver(string id)
        {
            T_AVUKAT avukat = db.T_AVUKAT.Where(t => t.id == site.CurrId).FirstOrDefault();
            site.M_SIKAYET_LISTE = (from a in db.T_AVUKAT_BRANS
                                    from b in db.T_AVUKAT_SEHIR
                                    join c in db.T_SIKAYET on a.brans_id equals c.brans_id
                                    join d in db.T_SIKAYET on b.il_id equals d.il_id
                                    where c.id == d.id
                                    && c.durumu == 0
                                    && avukat.id == a.avukat_id
                                    && avukat.id == b.avukat_id
                                    select new T_SIKAYET_LISTE
                                    {
                                        id = d.id,
                                        dosya_kod = d.dosya_kod,
                                        adi_soyadi = d.adi_soyadi,
                                        il_adi = d.T_SEHIR.il_adi,
                                        brans_adi = d.T_BRANS.brans_adi,
                                        sikayet_tarihi = d.sikayet_tarihi,
                                        teklif_sayisi = d.T_SIKAYET_AVUKAT.Count(),
                                        teklif_verdimi = d.T_SIKAYET_AVUKAT.Where(t => t.avukat_id == avukat.id).Count()
                                    }).FirstOrDefault();

            if (site.M_SIKAYET_LISTE == null)
            {
                ViewBag.Mesaj = "Dosya bulunamadı. Lütfen size uygun listelediğimiz dosyalardan birini seçiniz.";
                ViewBag.Uygunmu = "1";
            }
            else
            {
                site.M_SIKAYET = db.T_SIKAYET.Where(t => t.dosya_kod == id).FirstOrDefault();

                site.KrediParams = (Convert.ToInt16(_common.KacinciGun(site.M_SIKAYET.sikayet_tarihi)) >= Convert.ToInt16(_common.ParamKey("avukat_sikayet_teklif_bedava", "avukat_sikayet_teklif_bedava")) ? "0" : Convert.ToString(_common.ParamKey("avukat_sikayet_teklif_ver", _common.KacinciGun(site.M_SIKAYET.sikayet_tarihi))));
                ViewBag.Mesaj = KrediIslemleri(avukat.id, site.M_SIKAYET.id, 1, DateTime.Now, Convert.ToInt64(_common.ParamKey("avukat_sikayet_okuma", "avukat_sikayet_okuma")), id);
            }

            return View(site);
        }

        [ErrorLog]
        [ValidateInput(false)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "0")]
        public ActionResult Teklif_Ver(string id, string teklif_aciklama)
        {
            string pnr = "";
            T_AVUKAT avukat = db.T_AVUKAT.Where(t => t.id == site.CurrId).FirstOrDefault();
            site.M_SIKAYET_LISTE = (from a in db.T_AVUKAT_BRANS
                                    from b in db.T_AVUKAT_SEHIR
                                    join c in db.T_SIKAYET on a.brans_id equals c.brans_id
                                    join d in db.T_SIKAYET on b.il_id equals d.il_id
                                    where c.id == d.id
                                    && c.durumu == 0
                                    && avukat.id == a.avukat_id
                                    && avukat.id == b.avukat_id
                                    select new T_SIKAYET_LISTE
                                    {
                                        id = d.id,
                                        dosya_kod = d.dosya_kod,
                                        adi_soyadi = d.adi_soyadi,
                                        il_adi = d.T_SEHIR.il_adi,
                                        brans_adi = d.T_BRANS.brans_adi,
                                        sikayet_tarihi = d.sikayet_tarihi,
                                        teklif_sayisi = d.T_SIKAYET_AVUKAT.Count(),
                                        teklif_verdimi = d.T_SIKAYET_AVUKAT.Where(t => t.avukat_id == avukat.id).Count()
                                    }).FirstOrDefault();

            if (site.M_SIKAYET_LISTE == null)
            {
                ViewBag.Mesaj = "Dosya bulunamadı. Lütfen size uygun listelediğimiz dosyalardan birini seçiniz.";
                ViewBag.Uygunmu = "1";
            }
            else
            {
                pnr = _common.PnrGenerate();
                T_SIKAYET sikayet = db.T_SIKAYET.Where(t => t.dosya_kod == id).FirstOrDefault();
                T_SIKAYET_AVUKAT sikayet_avukat = db.T_SIKAYET_AVUKAT.Where(t => t.avukat_id == avukat.id).FirstOrDefault();
                if (sikayet_avukat == null)
                {
                    T_SIKAYET_AVUKAT avukat_teklif = new T_SIKAYET_AVUKAT();
                    avukat_teklif.avukat_id = avukat.id;
                    avukat_teklif.sikayet_id = sikayet.id;
                    avukat_teklif.sikayet_kodu = pnr;
                    avukat_teklif.teklif_aciklama = Sanitizer.GetSafeHtmlFragment(teklif_aciklama);
                    avukat_teklif.durumu = 5;
                    avukat_teklif.teklif_tarihi = DateTime.Now;
                    db.T_SIKAYET_AVUKAT.Add(avukat_teklif);
                    db.SaveChanges();
                }

                ViewBag.Mesaj = KrediIslemleri(avukat.id, sikayet.id, 5, DateTime.Now, Convert.ToInt64(_common.ParamKey("avukat_sikayet_teklif_ver", _common.KacinciGun(sikayet.sikayet_tarihi))), id);

                _mailing.MailSender(sikayet.e_posta_adresi, "Avukatlik.net - Teklif Bilgilendirme", "Merhaba, <br /><br /> Dosyanız avukatımız tarafından incelendi ve teklif gönderdi. Dosya takip numaranız : <b>" + sikayet.dosya_kod + "</b> .Dosyanızı takibini (Teklif ve Dosya bilgilerinizi) <a href='" + onlinelink + "/dosya-bul?dosya_kod=" + sikayet.dosya_kod + "&e_posta_adresi=" + sikayet.e_posta_adresi + "' target='_blank'>buradan</a> kontrol edebilirsiniz.<br /><br /><b>Not:</b> Dosyanız diğer avukatlarımız tarafından da incelenip size teklif göndereceklerdir. Birkaç gün beklemeniz yerinde olacaktır.<br /><br />İyi günler dileriz.", true);
            }

            return Redirect("/avukat/bilgilendirme/" + pnr);
        }

        [ErrorLog]
        [Authorize(Roles = "0")]
        public ActionResult Teklif_Duzenle(string id)
        {
            T_AVUKAT avukat = db.T_AVUKAT.Where(t => t.id == site.CurrId).FirstOrDefault();
            site.M_SIKAYET_LISTE = (from a in db.T_AVUKAT_BRANS
                                    from b in db.T_AVUKAT_SEHIR
                                    join c in db.T_SIKAYET on a.brans_id equals c.brans_id
                                    join d in db.T_SIKAYET on b.il_id equals d.il_id
                                    where c.id == d.id
                                    && c.durumu == 0
                                    && avukat.id == a.avukat_id
                                    && avukat.id == b.avukat_id
                                    select new T_SIKAYET_LISTE
                                    {
                                        id = d.id,
                                        dosya_kod = d.dosya_kod,
                                        adi_soyadi = d.adi_soyadi,
                                        il_adi = d.T_SEHIR.il_adi,
                                        brans_adi = d.T_BRANS.brans_adi,
                                        sikayet_tarihi = d.sikayet_tarihi,
                                        teklif_sayisi = d.T_SIKAYET_AVUKAT.Count(),
                                        teklif_verdimi = d.T_SIKAYET_AVUKAT.Where(t => t.avukat_id == avukat.id).Count()
                                    }).FirstOrDefault();

            if (site.M_SIKAYET_LISTE == null)
            {
                ViewBag.Mesaj = "Dosya bulunamadı. Lütfen size uygun listelediğimiz dosyalardan birini seçiniz.";
                ViewBag.Uygunmu = "1";
            }
            else
            {
                site.M_SIKAYET_AVUKAT = db.T_SIKAYET_AVUKAT.Where(t => t.sikayet_kodu == id).FirstOrDefault();
                site.M_SIKAYET = db.T_SIKAYET.Where(t => t.id == site.M_SIKAYET_AVUKAT.sikayet_id).FirstOrDefault();
                //site.KrediParams = Convert.ToString(ParamKey("avukat_sikayet_teklif_ver", KacinciGun(site.M_SIKAYET.sikayet_tarihi)));
                //ViewBag.Mesaj = KrediIslemleri(avukat.id, site.M_SIKAYET.id, 1, DateTime.Now, Convert.ToInt64(ParamKey("avukat_sikayet_okuma", "avukat_sikayet_okuma")), id);
            }

            return View(site);
        }

        [ErrorLog]
        [ValidateInput(false)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "0")]
        public ActionResult Teklif_Duzenle(string id, string teklif_aciklama)
        {
            T_AVUKAT avukat = db.T_AVUKAT.Where(t => t.id == site.CurrId).FirstOrDefault();
            site.M_SIKAYET_LISTE = (from a in db.T_AVUKAT_BRANS
                                    from b in db.T_AVUKAT_SEHIR
                                    join c in db.T_SIKAYET on a.brans_id equals c.brans_id
                                    join d in db.T_SIKAYET on b.il_id equals d.il_id
                                    where c.id == d.id
                                    && c.durumu == 0
                                    && avukat.id == a.avukat_id
                                    && avukat.id == b.avukat_id
                                    select new T_SIKAYET_LISTE
                                    {
                                        id = d.id,
                                        dosya_kod = d.dosya_kod,
                                        adi_soyadi = d.adi_soyadi,
                                        il_adi = d.T_SEHIR.il_adi,
                                        brans_adi = d.T_BRANS.brans_adi,
                                        sikayet_tarihi = d.sikayet_tarihi,
                                        teklif_sayisi = d.T_SIKAYET_AVUKAT.Count(),
                                        teklif_verdimi = d.T_SIKAYET_AVUKAT.Where(t => t.avukat_id == avukat.id).Count()
                                    }).FirstOrDefault();

            if (site.M_SIKAYET_LISTE == null)
            {
                ViewBag.Mesaj = "Dosya bulunamadı. Lütfen size uygun listelediğimiz dosyalardan birini seçiniz.";
                ViewBag.Uygunmu = "1";
            }
            else
            {
                T_SIKAYET_AVUKAT sikayet_avukat = db.T_SIKAYET_AVUKAT.Where(t => t.sikayet_kodu == id).FirstOrDefault();
                sikayet_avukat.teklif_aciklama = Sanitizer.GetSafeHtmlFragment(teklif_aciklama);
                db.SaveChanges();
                T_SIKAYET sikayet = db.T_SIKAYET.Where(t => t.id == sikayet_avukat.sikayet_id).FirstOrDefault();

                ViewBag.Mesaj = KrediIslemleri(avukat.id, sikayet.id, 5, DateTime.Now, Convert.ToInt64(_common.ParamKey("avukat_sikayet_teklif_ver", _common.KacinciGun(sikayet.sikayet_tarihi))), id);
                _mailing.MailSender(sikayet.e_posta_adresi, "Avukatlik.net - Teklif Bilgilendirme (Güncelleme)", "Merhaba, <br /><br /> Dosyanız avukatımız tarafından <b>tekrar</b> incelendi ve size gönderdi. Dosya takip numaranız : <b>" + sikayet.dosya_kod + "</b> .Dosyanızı takibini (Teklif ve Dosya bilgilerinizi) <a href='" + onlinelink + "/dosya-bul?dosya_kod=" + sikayet.dosya_kod + "&e_posta_adresi=" + sikayet.e_posta_adresi + "' target='_blank'>buradan</a> kontrol edebilirsiniz.<br /><br /><b>Not:</b> Dosyanız diğer avukatlarımız tarafından da incelenip size teklif göndereceklerdir. Birkaç gün beklemeniz yerinde olacaktır.<br /><br />İyi günler dileriz.", true);
            }

            return Redirect("/avukat/bilgilendirme/");
        }

        [ErrorLog]
        [Authorize(Roles = "0")]
        public ActionResult Bilgilendirme(string id)
        {


            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "0")]
        public ActionResult Kullanici_Bilgilerim()
        {
            site.M_AVUKAT = db.T_AVUKAT.Where(t => t.id == site.CurrId).FirstOrDefault();
            site.M_AVUKAT_KREDI_LOGS = db.T_AVUKAT_KREDI_LOG.Where(t => t.avukat_id == site.CurrId).OrderByDescending(t => t.islem_tarihi).Take(10).ToList();
            site.M_AVUKAT_DOSYA_ISLEMS = db.T_AVUKAT_DOSYA_ISLEM.Where(t => t.avukat_id == site.CurrId).OrderByDescending(t => t.islem_tarih).Take(10).ToList();
            site.AnaSayfaKrediEkle = Convert.ToString(_common.ParamKey("avukat_ana_sayfaya_ekleme_kredi", "avukat_ana_sayfaya_ekleme_kredi"));
            site.SehirKrediEkle = Convert.ToString(_common.ParamKey("avukat_sehir_ekleme_kredi", "avukat_sehir_ekleme_kredi"));
            site.BransKrediEkle = Convert.ToString(_common.ParamKey("avukat_brans_ekleme_kredi", "avukat_brans_ekleme_kredi"));
            site.AnaSayfaKalmaSuresi = Convert.ToString(_common.ParamKey("avukat_profil_anasayfada_kalma_suresi", "avukat_profil_anasayfada_kalma_suresi"));

            site.AnaSayfadaVar = (db.T_SLIDER_AVUKAT.Where(t => t.avukat_id == site.CurrId && t.baslama_tarihi >= DateTime.Today && t.bitis_tarihi <= DateTime.Today).Count() > 0 ? true : false);
            site.EklenecekBransYok = (site.M_AVUKAT.brans_ekleme_sayisi >= db.T_BRANS.Count() ? true : false);
            site.EklenecekSehirYok = (site.M_AVUKAT.sehir_ekleme_sayisi >= db.T_SEHIR.Count() ? true : false);


            return View(site);
        }

        [ErrorLog]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "0")]
        public ActionResult Arkadasima_Gonder(string e_posta_adresi)
        {
            if (e_posta_adresi != null)
            {
                foreach (var item in e_posta_adresi.Split(','))
                {
                    _mailing.MailSender(item, "Avukatlik.net - Arkadaş Önerisi", "Merhaba, ben " + site.CurrAdiSoyadi + ", sana Avukatlik.net adresine üye olman için aşağıdaki linki yolluyorum. Benim referansımla üye olursan sana " + Convert.ToString(_common.ParamKey("kredi_limit_tavsiye_edilen", "kredi_limit_tavsiye_edilen")) + " kredi hediye verilecek. İlk kayıt esnasında " + Convert.ToString(_common.ParamKey("kredi_limit", "kredi_limit")) + " hediye kredi de Avukatlik.net den hediye.<br /><br />Bu linki <a href='" + onlinelink + "/avukat-kayit/" + site.CurrAvukatKod + "' class=\"text-blue\" target=\"_blank\">tıklayarak</a> üye olabilirsin, <br /><br />İyi günler dilerim.", true);
                }
            }

            return Redirect("/avukat/kullanici-bilgilerim");
        }

        [ErrorLog]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "0")]
        public ActionResult Kredi_Kullandirim(string btn_kredi_kullandirim)
        {
            T_AVUKAT avukat = db.T_AVUKAT.Where(t => t.id == site.CurrId).FirstOrDefault();

            if (btn_kredi_kullandirim == "avukat_ana_sayfaya_ekleme_kredi")
            {
                T_SLIDER_AVUKAT slider = new T_SLIDER_AVUKAT();
                slider.avukat_id = avukat.id;
                slider.baslama_tarihi = DateTime.Today;
                slider.bitis_tarihi = DateTime.Today.AddDays(Convert.ToInt16(_common.ParamKey("avukat_profil_anasayfada_kalma_suresi", "avukat_profil_anasayfada_kalma_suresi")));
                slider.sira_no = 0;
                slider.kayit_tarihi = DateTime.Now;
                db.T_SLIDER_AVUKAT.Add(slider);
                KrediLog(avukat.id, Convert.ToInt16(_common.ParamKey(btn_kredi_kullandirim, btn_kredi_kullandirim)), DateTime.Now, "ana sayfaya profil ekleme");
            }
            else if (btn_kredi_kullandirim == "avukat_sehir_ekleme_kredi")
            {
                avukat.sehir_ekleme_sayisi += 1;
                KrediLog(avukat.id, Convert.ToInt16(_common.ParamKey(btn_kredi_kullandirim, btn_kredi_kullandirim)), DateTime.Now, "şehir ekleme");
            }
            else if (btn_kredi_kullandirim == "avukat_brans_ekleme_kredi")
            {
                avukat.brans_ekleme_sayisi += 1;
                KrediLog(avukat.id, Convert.ToInt16(_common.ParamKey(btn_kredi_kullandirim, btn_kredi_kullandirim)), DateTime.Now, "branş ekleme");
            }

            db.SaveChanges();

            return Redirect("/avukat/kullanici-bilgilerim");
        }

        [ErrorLog]
        [Authorize(Roles = "0")]
        public ActionResult Kullanici_Duzenle()
        {
            site.M_AVUKAT = db.T_AVUKAT.Where(t => t.id == site.CurrId).FirstOrDefault();
            site.M_BRANSS = db.T_BRANS.ToList();
            site.M_SEHIRS = db.T_SEHIR.ToList();

            if (Convert.ToInt16(_common.ParamKey("avukat_brans_secim", "avukat_brans_secim")) > 0)
                site.BransLimit = "En fazla " + (Convert.ToInt16(_common.ParamKey("avukat_brans_secim", "avukat_brans_secim")) + site.M_AVUKAT.brans_ekleme_sayisi) + " branş ekleyebilirsiniz.";
            else
                site.BransLimit = "Birden çok branş ekleyebilirsiniz.";

            if (Convert.ToInt16(_common.ParamKey("avukat_sehir_secim", "avukat_sehir_secim")) > 0)
                site.SehirLimit = "En fazla " + (Convert.ToInt16(_common.ParamKey("avukat_sehir_secim", "avukat_sehir_secim")) + site.M_AVUKAT.sehir_ekleme_sayisi) + " şehir ekleyebilirsiniz.";
            else
                site.SehirLimit = "Birden çok şehir ekleyebilirsiniz.";

            return View(site);
        }

        [ErrorLog]
        [ValidateInput(false)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "0")]
        public ActionResult Kullanici_Duzenle(T_AVUKAT avukats, int[] brans_id, int[] ils_id)
        {
            site.ErrorStr = "";
            T_AVUKAT avukat = db.T_AVUKAT.Where(t => t.id == site.CurrId).FirstOrDefault();

            int cep_telefonu_cont = db.T_AVUKAT.Where(t => t.cep_telefonu == avukats.cep_telefonu && t.cep_telefonu != avukat.cep_telefonu).Count();
            if (cep_telefonu_cont > 0)
                site.ErrorStr += "<div class='text-red mb-2'>Cep telefonu sistemde mevcut. Lütfen başka bir telefon numarası yazınız.</div>";

            int e_posta_cont = db.T_AVUKAT.Where(t => t.e_posta_adresi == avukats.e_posta_adresi && t.e_posta_adresi != avukat.e_posta_adresi).Count();
            if (e_posta_cont > 0)
                site.ErrorStr += "<div class='text-red mb-2'>E-posta adresi sistemde mevcut. Lütfen başka bir e-posta adresi yazınız.</div>";

            if (Convert.ToInt16(_common.ParamKey("avukat_brans_secim", "avukat_brans_secim")) > 0 && brans_id.Length > (Convert.ToInt16(_common.ParamKey("avukat_brans_secim", "avukat_brans_secim")) + avukat.brans_ekleme_sayisi))
                site.ErrorStr += "<div class='text-red mb-2'>En fazla " + (Convert.ToInt16(_common.ParamKey("avukat_brans_secim", "avukat_brans_secim")) + avukat.brans_ekleme_sayisi) + " branş ekleyebilirsiniz.</div>";

            if (Convert.ToInt16(_common.ParamKey("avukat_sehir_secim", "avukat_sehir_secim")) > 0 && ils_id.Length > (Convert.ToInt16(_common.ParamKey("avukat_sehir_secim", "avukat_sehir_secim")) + avukat.sehir_ekleme_sayisi))
                site.ErrorStr += "<div class='text-red mb-2'>En fazla " + (Convert.ToInt16(_common.ParamKey("avukat_sehir_secim", "avukat_sehir_secim")) + avukat.sehir_ekleme_sayisi) + " şehir ekleyebilirsiniz.</div>";

            if (site.ErrorStr != "")
            {
                site.M_AVUKAT = db.T_AVUKAT.Where(t => t.id == site.CurrId).FirstOrDefault();
                site.M_BRANSS = db.T_BRANS.ToList();
                site.M_SEHIRS = db.T_SEHIR.ToList();
                return View(site);
            }

            avukat.adi_soyadi = Sanitizer.GetSafeHtmlFragment(avukats.adi_soyadi);
            avukat.cep_telefonu = Sanitizer.GetSafeHtmlFragment(avukats.cep_telefonu);
            avukat.e_posta_adresi = Sanitizer.GetSafeHtmlFragment(avukats.e_posta_adresi);
            avukat.adresi = Sanitizer.GetSafeHtmlFragment(avukats.adresi);
            avukat.il_id = avukats.il_id;
            avukat.bagli_olunan_baro = Sanitizer.GetSafeHtmlFragment(avukats.bagli_olunan_baro);
            avukat.ozgecmis = Sanitizer.GetSafeHtmlFragment(avukats.ozgecmis);

            db.T_AVUKAT_BRANS.RemoveRange(avukat.T_AVUKAT_BRANS.ToList());
            foreach (var item in brans_id)
            {

                T_AVUKAT_BRANS av_brans = new T_AVUKAT_BRANS();
                av_brans.avukat_id = avukat.id;
                av_brans.brans_id = item;
                db.T_AVUKAT_BRANS.Add(av_brans);
            }

            db.T_AVUKAT_SEHIR.RemoveRange(avukat.T_AVUKAT_SEHIR.ToList());
            foreach (var item in ils_id)
            {
                T_AVUKAT_SEHIR av_sehir = new T_AVUKAT_SEHIR();
                av_sehir.avukat_id = avukat.id;
                av_sehir.il_id = item;
                db.T_AVUKAT_SEHIR.Add(av_sehir);
            }

            db.SaveChanges();

            return Redirect("/avukat/kullanici-bilgilerim");
        }

        [ErrorLog]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "0")]
        public ActionResult Profil_Resim_Degistir(HttpPostedFileBase avukat_resim)
        {
            if (avukat_resim != null)
            {
                T_AVUKAT avukat = db.T_AVUKAT.Where(t => t.id == site.CurrId).FirstOrDefault();
                if (avukat.avukat_resim != null && System.IO.File.Exists(Server.MapPath("~/Upload/ProfilResim/" + avukat.avukat_resim)) == true)
                {
                    System.IO.File.Delete(Server.MapPath("~/Upload/ProfilResim/" + avukat.avukat_resim));
                    System.IO.File.Delete(Server.MapPath("~/Upload/ProfilResim/Thumb/" + avukat.avukat_resim));
                }
                string filename = avukat.avukat_code + Path.GetExtension(avukat_resim.FileName);
                _common.ResizeStream(400, avukat_resim.InputStream, Server.MapPath("~/Upload/ProfilResim/"), Path.GetFileName(filename));
                _common.ResizeStream(90, avukat_resim.InputStream, Server.MapPath("~/Upload/ProfilResim/Thumb/"), Path.GetFileName(filename));
                avukat.avukat_resim = filename;
                db.SaveChanges();

                site.CurrResim = filename;
            }

            return Redirect("/avukat/kullanici-bilgilerim");
        }

        [ErrorLog]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "0")]
        public ActionResult Sifre_Degistir(string sifre, string yeni_sifre, string yeni_sifre_tekrar)
        {
            site.ErrorStr = "";
            T_AVUKAT avukat = db.T_AVUKAT.Where(t => t.id == site.CurrId).FirstOrDefault();

            if (_common.GetHashString(sifre) != _common.GetHashString(avukat.sifre))
                site.ErrorStr += "<div class='text-red mb-5'>Mevcut şifreniz hatalı !</div>";

            if (yeni_sifre != yeni_sifre_tekrar)
                site.ErrorStr += "<div class='text-red mb-5'>Yeni şifre ve tekrar şifre birbirinden farklı !</div>";

            if (yeni_sifre.Length < 5 || yeni_sifre.Length > 10)
                site.ErrorStr += "<div class='text-red mb-5'>Şifreniz en az 5 en fazla 10 karakter olmalıdır.</div>";

            if (site.ErrorStr != "")
            {
                site.M_AVUKAT = db.T_AVUKAT.Where(t => t.id == site.CurrId).FirstOrDefault();
                site.M_BRANSS = db.T_BRANS.ToList();
                site.M_SEHIRS = db.T_SEHIR.ToList();
                return View(site);
            }

            avukat.sifre = _common.GetHashString(yeni_sifre);
            db.SaveChanges();

            //Avukata mail gönder
            _mailing.MailSender(avukat.e_posta_adresi, "Avukatlik.net - Şifre Değişikliği", "Merhaba, şifreniz başarıyla değiştirildi. <a href='" + onlinelink + "' class=\"text-blue\" target=\"_blank\">Online işlemler ekranına</a> giriş bilgileriniz, <br /><br /><b>E-posta : </b>" + avukat.e_posta_adresi + "<br /><b>Şifreniz : </b>" + yeni_sifre + "<br /><br />İyi günler dileriz.", true);

            return Redirect("/avukat/kullanici-bilgilerim");
        }

        [ErrorLog]
        [Authorize(Roles = "0")]
        public ActionResult Dosya_Detay(string id)
        {
            site.M_SIKAYET = db.T_SIKAYET.Where(t => t.dosya_kod == id && t.T_AVUKAT_DOSYA_ISLEM.Where(a => a.avukat_id == site.CurrId && a.dosya_islem == 1).Count() > 0).FirstOrDefault(); // Okuduysa
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "0")]
        public ActionResult Kredi_Islemleri()
        {
            site.M_AVUKAT_KREDI_LOGS = db.T_AVUKAT_KREDI_LOG.Where(t => t.avukat_id == site.CurrId).OrderByDescending(t => t.islem_tarihi).ToList();
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "0")]
        public ActionResult Dosya_Islemleri()
        {
            site.M_AVUKAT_DOSYA_ISLEMS = db.T_AVUKAT_DOSYA_ISLEM.Where(t => t.avukat_id == site.CurrId).OrderByDescending(t => t.islem_tarih).ToList();
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "0")]
        public ActionResult Odeme_Islemleri()
        {
            site.M_KREDI_PLANS = db.T_KREDI_PLAN.ToList();
            site.M_AVUKAT_KREDI_LOGS = db.T_AVUKAT_KREDI_LOG.Where(t => t.avukat_id == site.CurrId && t.islem == "+").OrderByDescending(t => t.islem_tarihi).ToList();
            return View(site);
        }

        //--------------------------------------------------- Ortak kullanım alanı

        [ErrorLog]
        private string KrediIslemleri(long avukat_id, long sikayet_id, int dosya_islem, DateTime tarih, long kredi, string id)
        {
            string ret = "";

            T_AVUKAT_DOSYA_ISLEM dosya_islemi_varmi = db.T_AVUKAT_DOSYA_ISLEM.Where(t => avukat_id == site.CurrId && t.sikayet_id == sikayet_id && t.dosya_islem == dosya_islem).FirstOrDefault();
            if (dosya_islemi_varmi == null)
            {
                //İşlem Log Kayıt
                T_AVUKAT_DOSYA_ISLEM dosya_islemi = new T_AVUKAT_DOSYA_ISLEM();
                dosya_islemi.avukat_id = avukat_id;
                dosya_islemi.sikayet_id = sikayet_id;
                dosya_islemi.dosya_islem = dosya_islem; //Dosya İşlemi 1 okuma. 5 ise teklif verme
                dosya_islemi.dosya_islem_sayisi += 0 + 1; //Her okumaya girdiğinde veya teklif verme işlemi yaptığında yeniden sayı artacak
                dosya_islemi.islem_tarih = tarih;
                db.T_AVUKAT_DOSYA_ISLEM.Add(dosya_islemi);
                //Okuma kredisi ve Yazma kredisi
                T_AVUKAT avukat = db.T_AVUKAT.Where(t => t.id == avukat_id).FirstOrDefault();
                avukat.kredi -= kredi;
                //Görüntüleme Log Kayıt
                T_AVUKAT_KREDI_LOG kredi_log = new T_AVUKAT_KREDI_LOG();
                kredi_log.avukat_id = avukat_id;
                kredi_log.islem = "-";
                kredi_log.kredi_limit = kredi;
                kredi_log.islem_tarihi = tarih;
                kredi_log.aciklama = tarih + " tarihinde " + id + " kodlu dosya için " + kredi + " kredilik " + Convert.ToString(_common.ParamKey("dosya_durum", dosya_islem.ToString())) + " işlemi yapıldı.";
                db.T_AVUKAT_KREDI_LOG.Add(kredi_log);

                ret = "Yapılan işlem için hesabınızdan " + kredi + " kredi düşmüştür.";
            }
            else
            {
                dosya_islemi_varmi.dosya_islem_sayisi += 1; //Her okumaya girdiğinde veya teklif verme işlemi yaptığında yeniden sayı artacak
            }

            db.SaveChanges();

            return ret;
        }

        private void KrediLog(long avukat_id, long kredi, DateTime tarih, string param_islem)
        {
            T_AVUKAT_KREDI_LOG kredi_log = new T_AVUKAT_KREDI_LOG();
            kredi_log.avukat_id = avukat_id;
            kredi_log.islem = "-";
            kredi_log.kredi_limit = kredi;
            kredi_log.islem_tarihi = tarih;
            kredi_log.aciklama = tarih + " tarihinde " + param_islem + " işlemi için " + kredi + " kredilik işlemi yapıldı.";
            db.T_AVUKAT_KREDI_LOG.Add(kredi_log);
        }

    }
}
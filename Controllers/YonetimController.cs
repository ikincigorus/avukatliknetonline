﻿using AvukatliknetOnline.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AvukatliknetOnline.Controllers
{
    public class YonetimController : Controller
    {
        // GET: Yonetim

        [ErrorLog]
        [Authorize(Roles = "10")]
        public ActionResult Dashboard()
        {
            return View();
        }
    }
}
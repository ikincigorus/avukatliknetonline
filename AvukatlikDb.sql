USE [dbAvukatlik]
GO
/****** Object:  Table [dbo].[T_AVUKAT]    Script Date: 28.12.2020 21:46:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_AVUKAT](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[avukat_code] [nvarchar](40) NULL,
	[avukat_resim] [nvarchar](50) NULL,
	[adi_soyadi] [nvarchar](50) NULL,
	[cep_telefonu] [nvarchar](20) NULL,
	[e_posta_adresi] [nvarchar](150) NULL,
	[adresi] [nvarchar](100) NULL,
	[il_id] [int] NULL,
	[bagli_olunan_baro] [nvarchar](50) NULL,
	[sifre] [nvarchar](100) NULL,
	[ozgecmis] [nvarchar](max) NULL,
	[kredi] [bigint] NULL,
	[goruntuleme_sayisi] [bigint] NULL CONSTRAINT [DF_T_AVUKAT_goruntuleme_sayisi]  DEFAULT ((0)),
	[bizi_nereden_duydunuz] [nvarchar](3) NULL,
	[avukat_oneren] [nvarchar](50) NULL,
	[aktiflik_puani] [bigint] NULL,
	[uyelik_tarihi] [smalldatetime] NULL,
	[brans_ekleme_sayisi] [int] NULL CONSTRAINT [DF_T_AVUKAT_brans_ekleme_sayisi]  DEFAULT ((0)),
	[sehir_ekleme_sayisi] [int] NULL CONSTRAINT [DF_T_AVUKAT_sehir_ekleme_sayisi]  DEFAULT ((0)),
	[durumu] [int] NULL CONSTRAINT [DF_T_AVUKAT_durumu]  DEFAULT ((0)),
	[yonetici] [int] NULL CONSTRAINT [DF_T_AVUKAT_manage]  DEFAULT ((0)),
 CONSTRAINT [PK_T_AVUKAT] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[T_AVUKAT_BRANS]    Script Date: 28.12.2020 21:46:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_AVUKAT_BRANS](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[avukat_id] [bigint] NULL,
	[brans_id] [int] NULL,
 CONSTRAINT [PK_T_AVUKAT_BRANS] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[T_AVUKAT_DOSYA_ISLEM]    Script Date: 28.12.2020 21:46:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_AVUKAT_DOSYA_ISLEM](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[avukat_id] [bigint] NULL,
	[sikayet_id] [bigint] NULL,
	[dosya_islem] [int] NULL,
	[dosya_islem_sayisi] [int] NULL CONSTRAINT [DF_T_AVUKAT_DOSYA_ISLEM_dosya_islem_sayisi]  DEFAULT ((0)),
	[islem_tarih] [smalldatetime] NULL,
 CONSTRAINT [PK_T_AVUKAT_DOSYA_ISLEM] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[T_AVUKAT_KREDI_LOG]    Script Date: 28.12.2020 21:46:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_AVUKAT_KREDI_LOG](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[avukat_id] [bigint] NULL,
	[odeme_id] [nvarchar](15) NULL,
	[kredi_limit] [bigint] NULL,
	[islem] [nvarchar](1) NULL,
	[islem_tarihi] [smalldatetime] NULL,
	[aciklama] [nvarchar](500) NULL,
 CONSTRAINT [PK_TBL_KREDI_LOG] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[T_AVUKAT_SEHIR]    Script Date: 28.12.2020 21:46:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_AVUKAT_SEHIR](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[avukat_id] [bigint] NULL,
	[il_id] [int] NULL,
 CONSTRAINT [PK_T_AVUKAT_SEHIR] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[T_BRANS]    Script Date: 28.12.2020 21:46:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_BRANS](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[brans_adi] [nvarchar](50) NULL,
	[aciklama] [nvarchar](300) NULL,
 CONSTRAINT [PK_T_BRANS] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[T_KREDI_PLAN]    Script Date: 28.12.2020 21:46:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_KREDI_PLAN](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[kredi] [int] NULL,
	[fiyat] [decimal](18, 2) NULL,
	[gecerlilik_baslangic] [smalldatetime] NULL,
	[gecerlilik_bitis] [smalldatetime] NULL,
	[ap] [bit] NULL,
	[kayit_tarihi] [smalldatetime] NULL,
 CONSTRAINT [PK_T_KREDI_PLAN] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[T_PARAMS]    Script Date: 28.12.2020 21:46:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_PARAMS](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[grup] [nvarchar](50) NULL,
	[keys] [nvarchar](50) NULL,
	[value] [nvarchar](50) NULL,
	[sira_no] [int] NULL CONSTRAINT [DF_T_PARAMS_sira_no]  DEFAULT ((0)),
 CONSTRAINT [PK_T_PARAMS] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[T_SEHIR]    Script Date: 28.12.2020 21:46:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_SEHIR](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[il_id] [int] NULL,
	[il_adi] [nvarchar](50) NULL,
	[il_oncelik] [int] NULL,
 CONSTRAINT [PK_T_SEHIR] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[T_SIKAYET]    Script Date: 28.12.2020 21:46:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_SIKAYET](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[dosya_kod] [nvarchar](20) NULL,
	[adi_soyadi] [nvarchar](50) NULL,
	[cep_telefonu] [nvarchar](20) NULL,
	[e_posta_adresi] [nvarchar](150) NULL,
	[il_id] [int] NULL,
	[brans_id] [int] NULL,
	[iletisim_tercihi] [int] NULL,
	[sikayet_aciklama] [nvarchar](max) NULL,
	[dosya1] [nvarchar](50) NULL,
	[dosya2] [nvarchar](50) NULL,
	[dosya3] [nvarchar](50) NULL,
	[dosya_kapat] [bit] NULL CONSTRAINT [DF_T_SIKAYET_dosya_kapat]  DEFAULT ((0)),
	[dosya_kapat_neden] [nvarchar](50) NULL,
	[sil_kodu] [nvarchar](10) NULL,
	[sil_hata_sayisi] [int] NULL CONSTRAINT [DF_T_SIKAYET_sil_hata_sayisi]  DEFAULT ((0)),
	[islem_tarihi] [smalldatetime] NULL,
	[durumu] [int] NULL CONSTRAINT [DF_T_SIKAYET_durumu]  DEFAULT ((0)),
	[sikayet_tarihi] [smalldatetime] NULL,
 CONSTRAINT [PK_T_SIKAYET] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[T_SIKAYET_AVUKAT]    Script Date: 28.12.2020 21:46:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_SIKAYET_AVUKAT](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[sikayet_kodu] [nvarchar](50) NULL,
	[sikayet_id] [bigint] NULL,
	[avukat_id] [bigint] NULL,
	[teklif_aciklama] [nvarchar](max) NULL,
	[teklif_tarihi] [smalldatetime] NULL,
	[durumu] [int] NULL,
 CONSTRAINT [PK_T_SIKAYET_AVUKAT] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[T_SLIDER_AVUKAT]    Script Date: 28.12.2020 21:46:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_SLIDER_AVUKAT](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[avukat_id] [bigint] NULL,
	[baslama_tarihi] [smalldatetime] NULL,
	[bitis_tarihi] [smalldatetime] NULL,
	[sira_no] [bigint] NULL CONSTRAINT [DF_T_SLIDER_AVUKAT_sira_no]  DEFAULT ((0)),
	[kayit_tarihi] [smalldatetime] NULL,
 CONSTRAINT [PK_T_SLIDER_AVUKAT] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[T_AVUKAT] ON 

INSERT [dbo].[T_AVUKAT] ([id], [avukat_code], [avukat_resim], [adi_soyadi], [cep_telefonu], [e_posta_adresi], [adresi], [il_id], [bagli_olunan_baro], [sifre], [ozgecmis], [kredi], [goruntuleme_sayisi], [bizi_nereden_duydunuz], [avukat_oneren], [aktiflik_puani], [uyelik_tarihi], [brans_ekleme_sayisi], [sehir_ekleme_sayisi], [durumu], [yonetici]) VALUES (2, N'e42a6b47-c049-4824-8406-4ceaa18970bc', N'e42a6b47-c049-4824-8406-4ceaa18970bc.jpg', N'Oğuzhan Arslan', N'5333468985', N'oguzhan1981@gmail.com', N'Üsküdar', 34, N'İstanbul Barosu', N'5245CD8648F8FF96D533117E3A288446A1E467EDB15EAB563493BE9901084276', N'
<div>
<div><b>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus vel ligula felis. Fusce convallis justo leo, et euismod nibh auctor quis. Suspendisse ante sapien, laoreet id est a, ornare pretium libero. Nunc nec ligula ut dolor commodo malesuada</b> quis nec mi. In et arcu sit amet nunc suscipit consequat gravida sed magna. Vestibulum auctor, nisi non sollicitudin tristique, metus mi tempor neque, ac tincidunt ante eros nec mauris. Aenean id maximus quam. Cras sit amet eleifend turpis. Praesent id est vitae libero elementum sagittis. Duis<font color="#ff0000"> ornare blandit odio, sit amet tristiq</font>ue metus tincidunt sit amet. Praesent quis molestie orci. Nunc dignissim eros et odio mattis posuere.<br>
<br>
</div>
<div>Morbi gravida accumsan semper. Aenean sit amet feugiat nisi. Nulla in dictum urna. Mauris vitae erat id purus lobortis sodales vel sit amet erat. Suspendisse non scelerisque sem. Donec dapibus urna in eros vestibulum, vitae tincidunt metus dictum. Vestibulum consequat, arcu ut placerat consectetur, sapien mi vulputate nisl, sit amet porta ligula odio sed arcu. Vestibulum non massa bibendum sem tincidunt venenatis. Phasellus felis orci, sollicitudin id dui eget, maximus interdum odio.<br>
<br>
</div>
<div>Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Morbi iaculis tincidunt lectus quis sagittis. Curabitur efficitur enim nec tellus venenatis, ac tempor dui pulvinar. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Mauris accumsan nunc in enim congue dapibus. Nullam fringilla erat mi, ut eleifend massa sollicitudin vitae. Mauris nec magna magna. Nunc venenatis mauris quis aliquam euismod. Nulla sem nibh, pellentesque vitae nulla eu, vehicula vulputate odio. Sed vel erat mi. Proin blandit ut lorem eget fermentum. Nunc lacus velit, ullamcorper pulvinar commodo eget, ullamcorper maximus sem. Maecenas eget gravida sem. Pellentesque facilisis sem vel tellus efficitur hendrerit. Phasellus convallis a orci ac posuere.<br>
<br>
</div>
<div>Praesent sed dolor vitae quam ornare interdum. Cras placerat vestibulum nisl sit amet semper. Donec elementum gravida arcu et molestie. Cras gravida magna non rutrum molestie. Mauris tellus magna, accumsan egestas faucibus in, mollis eu diam. Nullam lacinia scelerisque magna, vel vestibulum magna varius eget. Morbi sit amet finibus risus, eu lobortis eros. Vivamus erat arcu, condimentum a efficitur vitae, euismod sed erat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum egestas ac erat nec ultrices. Maecenas gravida libero sollicitudin, aliquet ipsum ut, interdum augue. Proin tortor felis, sollicitudin sollicitudin dignissim id, mollis sed neque. Cras pretium, nisi in cursus rutrum, purus tortor facilisis ligula, quis iaculis mi risus eget tortor. Nullam turpis quam, cursus fermentum dui at, mollis sagittis dui.<br>
<br>
</div>
<div>Nam varius ligula id lectus porta eleifend. Maecenas efficitur sem nec ultricies dapibus. Etiam posuere fermentum feugiat. Ut vehicula velit sagittis mi posuere, ut sollicitudin enim hendrerit. Aliquam erat volutpat. Mauris bibendum ultricies ligula id convallis. Ut commodo, lorem non ultricies molestie, mauris leo hendrerit ligula, eu pulvinar lacus tortor quis nisi. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Proin ornare laoreet justo vitae vestibulum. Vivamus vulputate auctor maximus.</div>
</div>
', 91, 0, NULL, NULL, 0, CAST(N'2020-12-02 17:08:00' AS SmallDateTime), 0, 0, 0, 0)
INSERT [dbo].[T_AVUKAT] ([id], [avukat_code], [avukat_resim], [adi_soyadi], [cep_telefonu], [e_posta_adresi], [adresi], [il_id], [bagli_olunan_baro], [sifre], [ozgecmis], [kredi], [goruntuleme_sayisi], [bizi_nereden_duydunuz], [avukat_oneren], [aktiflik_puani], [uyelik_tarihi], [brans_ekleme_sayisi], [sehir_ekleme_sayisi], [durumu], [yonetici]) VALUES (3, N'c6dec73bfcd6473eb58b3c6474124d86', NULL, N'Mevlüt Temiz', N'5333468985_1', N'oguzhan1981@gmail2.com', N'Kadıköy', 34, N'İstanbul Barosu', N'36F71AF9FE9BEC290CE5AE6C9364A2169CBE11AC39D29F722ACAC6083C2F739A', NULL, 100, 0, NULL, NULL, 0, CAST(N'2020-12-07 00:42:00' AS SmallDateTime), 1, 0, 0, 0)
SET IDENTITY_INSERT [dbo].[T_AVUKAT] OFF
SET IDENTITY_INSERT [dbo].[T_AVUKAT_BRANS] ON 

INSERT [dbo].[T_AVUKAT_BRANS] ([id], [avukat_id], [brans_id]) VALUES (12, 3, 1)
INSERT [dbo].[T_AVUKAT_BRANS] ([id], [avukat_id], [brans_id]) VALUES (13, 3, 2)
INSERT [dbo].[T_AVUKAT_BRANS] ([id], [avukat_id], [brans_id]) VALUES (14, 3, 3)
INSERT [dbo].[T_AVUKAT_BRANS] ([id], [avukat_id], [brans_id]) VALUES (15, 3, 4)
INSERT [dbo].[T_AVUKAT_BRANS] ([id], [avukat_id], [brans_id]) VALUES (20, 2, 2)
INSERT [dbo].[T_AVUKAT_BRANS] ([id], [avukat_id], [brans_id]) VALUES (21, 2, 4)
INSERT [dbo].[T_AVUKAT_BRANS] ([id], [avukat_id], [brans_id]) VALUES (22, 2, 9)
INSERT [dbo].[T_AVUKAT_BRANS] ([id], [avukat_id], [brans_id]) VALUES (23, 2, 18)
SET IDENTITY_INSERT [dbo].[T_AVUKAT_BRANS] OFF
SET IDENTITY_INSERT [dbo].[T_AVUKAT_DOSYA_ISLEM] ON 

INSERT [dbo].[T_AVUKAT_DOSYA_ISLEM] ([id], [avukat_id], [sikayet_id], [dosya_islem], [dosya_islem_sayisi], [islem_tarih]) VALUES (5, 2, 7, 1, 9, CAST(N'2020-12-13 17:06:00' AS SmallDateTime))
INSERT [dbo].[T_AVUKAT_DOSYA_ISLEM] ([id], [avukat_id], [sikayet_id], [dosya_islem], [dosya_islem_sayisi], [islem_tarih]) VALUES (6, 2, 7, 5, 1, CAST(N'2020-12-14 01:02:00' AS SmallDateTime))
INSERT [dbo].[T_AVUKAT_DOSYA_ISLEM] ([id], [avukat_id], [sikayet_id], [dosya_islem], [dosya_islem_sayisi], [islem_tarih]) VALUES (7, 2, 8, 1, 14, CAST(N'2020-12-19 16:54:00' AS SmallDateTime))
SET IDENTITY_INSERT [dbo].[T_AVUKAT_DOSYA_ISLEM] OFF
SET IDENTITY_INSERT [dbo].[T_AVUKAT_KREDI_LOG] ON 

INSERT [dbo].[T_AVUKAT_KREDI_LOG] ([id], [avukat_id], [odeme_id], [kredi_limit], [islem], [islem_tarihi], [aciklama]) VALUES (3, 2, NULL, 1, N'-', CAST(N'2020-12-13 17:06:00' AS SmallDateTime), N'13.12.2020 17:06:24 tarihinde XJ667647196 kodlu dosya için 1 kredilik Okuma işlemi yapıldı.')
INSERT [dbo].[T_AVUKAT_KREDI_LOG] ([id], [avukat_id], [odeme_id], [kredi_limit], [islem], [islem_tarihi], [aciklama]) VALUES (4, 2, NULL, 5, N'-', CAST(N'2020-12-14 01:02:00' AS SmallDateTime), N'14.12.2020 01:01:31 tarihinde AN729909047 kodlu dosya için 5 kredilik teklif verme işlemi yapıldı.')
INSERT [dbo].[T_AVUKAT_KREDI_LOG] ([id], [avukat_id], [odeme_id], [kredi_limit], [islem], [islem_tarihi], [aciklama]) VALUES (5, 2, NULL, 1, N'-', CAST(N'2020-12-19 16:54:00' AS SmallDateTime), N'19.12.2020 16:54:22 tarihinde HO122439662 kodlu dosya için 1 kredilik okuma işlemi yapıldı.')
SET IDENTITY_INSERT [dbo].[T_AVUKAT_KREDI_LOG] OFF
SET IDENTITY_INSERT [dbo].[T_AVUKAT_SEHIR] ON 

INSERT [dbo].[T_AVUKAT_SEHIR] ([id], [avukat_id], [il_id]) VALUES (8, 3, 6)
INSERT [dbo].[T_AVUKAT_SEHIR] ([id], [avukat_id], [il_id]) VALUES (9, 3, 34)
INSERT [dbo].[T_AVUKAT_SEHIR] ([id], [avukat_id], [il_id]) VALUES (10, 3, 35)
INSERT [dbo].[T_AVUKAT_SEHIR] ([id], [avukat_id], [il_id]) VALUES (13, 2, 34)
INSERT [dbo].[T_AVUKAT_SEHIR] ([id], [avukat_id], [il_id]) VALUES (14, 2, 54)
SET IDENTITY_INSERT [dbo].[T_AVUKAT_SEHIR] OFF
SET IDENTITY_INSERT [dbo].[T_BRANS] ON 

INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (1, N'Aile Hukuku', N'Medeni hukukun aile hukuku bölümü')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (2, N'Anayasa Hukuku', N'Anayasalar')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (3, N'Avrupa Birliği Hukuku', N'Avrupa birliği (AB) süreci ile ilgili kanunlar')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (4, N'Avukatlık Hukuku', N'Avukatlar hakkındaki mevzuat')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (5, N'Bankacılık Hukuku', N'Bankalar, banka kredi kartları hakkındaki kanunlar ve ilgili mevzuat')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (6, N'Basın Hukuku', N'Yayınlar ve Basın eserleri')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (7, N'Bilişim Hukuku', N'Teknoloji ve bilişim kanunları')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (8, N'Borçlar Hukuku', N'Borç ilişkileri hakkında mevzuat')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (9, N'Boşanma Hukuku', N'Evlenme ve boşanma mevzuatı')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (10, N'Çevre Hukuku', N'Çevresel düzenlemeler')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (11, N'Ceza Hukuku', N'Türk ceza hukuku')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (12, N'Ceza Usul Hukuku', N'Ceza yargılaması kanunları')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (13, N'Deniz Hukuku', N'Deniz mevzuatı')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (14, N'Devletler Hukuku', N'Uluslararası düzenlemeler')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (15, N'Eşya Hukuku', N'Taşınır ve taşınmaz mallar üzerindeki haklar')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (16, N'Fikir ve Sanat Eserleri Hukuku', N'Fikri haklar, telif ve sanat eserleri mevzuatı')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (17, N'Gayrımenkul Hukuku', N'Taşınmaz mevzuatı')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (18, N'Icra Hukuku', N'Cebri icra ve haciz işlemlerine dair kanunlar')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (19, N'Idare Hukuku', N'Yönetsel hukuk')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (20, N'Idari Yargılama Hukuku', N'İdare mahkemelerinde yargılama hukuku')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (21, N'Iflas Hukuku', N'Ticaret ile uğraşan tacirlerin ve şirketlerin iflası')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (22, N'Ihale Hukuku', N'Genel ve Kamu ihale mevzuatı')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (23, N'Imar Hukuku', N'İmar ve Belediye hukuku')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (24, N'Internet Hukuku', N'İnternet ile ilgili kanular')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (25, N'Islam Hukuku', N'İslam’da hukuk sistemi')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (26, N'Iş Hukuku', N'Eleman çalıştırılması, işveren ve işçi hukuku')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (27, N'Kamu Hukuku', N'Amme hukuku')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (28, N'Kat Mülkiyeti Hukuku', N'Kat mülkiyeti ve komşuluk kanunları')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (29, N'Kıymetli Evrak Hukuku', N'Çek, poliçe, bono (senet) hakkındaki mevzuat')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (30, N'Kira Hukuku', N'Kiracı ve kiraya verenlerin tabi olduğu yasal düzenlemeler')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (31, N'Mal Rejimleri Hukuku', N'Evlilik ve evlilikte mal rejimleri')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (32, N'Marka – Patent Hukuku', N'Markalar ve Patent, Marka Tescili sonrası hukuki haklar, ihtilaflar')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (33, N'Medeni Usul Hukuku', N'Medeni yargılama hukuku, mahkemelerin tabi olduğu hukuk')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (34, N'Miras Hukuku', N'Miras bırakan ve mirasçıların tabi olduğu yasal rejim')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (35, N'Rekabet Hukuku', N'Ticaret ve haksız rekabet')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (36, N'Reklam Hukuku', N'Reklam')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (37, N'Roma Hukuku', N'Çağdaş hukukun temelleri')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (38, N'Sağlık Hukuku', N'Hasta hakları, tıp ve sağlık hakkındaki kanunlar')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (39, N'Sermaye Piyasası Hukuku', N'SPK düzenlemeleri')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (40, N'Sigorta Hukuku', N'Sigortacılık mevzuatı')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (41, N'Şirketler Hukuku', N'Firmalar ve şirketlerin tabi olduğu hukuk dalı')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (42, N'Sosyal Güvenlik Hukuku', N'SGK (SSK) mevzuatı')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (43, N'Spor Hukuku', N'Spor müsabakaları hakkındaki kanuni düzenlemeler')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (44, N'Tazminat Hukuku', N'Trafik kazaları, kaza ve oluşan zarar ziyanların tazmini')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (45, N'Ticaret Hukuku', N'Ticaret hayatı ile ilgili mevzuat')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (46, N'Tüketici Hukuku', N'Tüketici sorunları ve ticari olmayan alışverişler dolayısıyla tüketici hakları')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (47, N'Vatandaşlık Hukuku', N'TC. Vatandaşlık ve Nüfus işlemlerine dair düzenlemeler')
INSERT [dbo].[T_BRANS] ([id], [brans_adi], [aciklama]) VALUES (48, N'Vergi Hukuku', N'Maliye, muhasebe ve genel vergi mevzuatı')
SET IDENTITY_INSERT [dbo].[T_BRANS] OFF
SET IDENTITY_INSERT [dbo].[T_KREDI_PLAN] ON 

INSERT [dbo].[T_KREDI_PLAN] ([id], [kredi], [fiyat], [gecerlilik_baslangic], [gecerlilik_bitis], [ap], [kayit_tarihi]) VALUES (1, 50, CAST(5.00 AS Decimal(18, 2)), CAST(N'2020-12-24 00:00:00' AS SmallDateTime), CAST(N'2021-12-24 00:00:00' AS SmallDateTime), 1, CAST(N'2020-12-24 00:00:00' AS SmallDateTime))
INSERT [dbo].[T_KREDI_PLAN] ([id], [kredi], [fiyat], [gecerlilik_baslangic], [gecerlilik_bitis], [ap], [kayit_tarihi]) VALUES (2, 100, CAST(10.00 AS Decimal(18, 2)), CAST(N'2020-12-24 00:00:00' AS SmallDateTime), CAST(N'2021-12-24 00:00:00' AS SmallDateTime), 1, CAST(N'2020-12-24 00:00:00' AS SmallDateTime))
INSERT [dbo].[T_KREDI_PLAN] ([id], [kredi], [fiyat], [gecerlilik_baslangic], [gecerlilik_bitis], [ap], [kayit_tarihi]) VALUES (3, 250, CAST(20.00 AS Decimal(18, 2)), CAST(N'2021-12-24 00:00:00' AS SmallDateTime), CAST(N'2021-12-24 00:00:00' AS SmallDateTime), 1, CAST(N'2020-12-24 00:00:00' AS SmallDateTime))
INSERT [dbo].[T_KREDI_PLAN] ([id], [kredi], [fiyat], [gecerlilik_baslangic], [gecerlilik_bitis], [ap], [kayit_tarihi]) VALUES (4, 500, CAST(40.00 AS Decimal(18, 2)), CAST(N'2021-12-24 00:00:00' AS SmallDateTime), CAST(N'2021-12-24 00:00:00' AS SmallDateTime), 1, CAST(N'2020-12-24 00:00:00' AS SmallDateTime))
SET IDENTITY_INSERT [dbo].[T_KREDI_PLAN] OFF
SET IDENTITY_INSERT [dbo].[T_PARAMS] ON 

INSERT [dbo].[T_PARAMS] ([id], [grup], [keys], [value], [sira_no]) VALUES (1, N'kredi_limit', N'kredi_limit', N'100', 0)
INSERT [dbo].[T_PARAMS] ([id], [grup], [keys], [value], [sira_no]) VALUES (2, N'sikayet_durum', N'0', N'Gönderildi', 1)
INSERT [dbo].[T_PARAMS] ([id], [grup], [keys], [value], [sira_no]) VALUES (3, N'sikayet_durum', N'1', N'Sonuçlandı', 2)
INSERT [dbo].[T_PARAMS] ([id], [grup], [keys], [value], [sira_no]) VALUES (4, N'bizi_nereden_duydunuz', N'1', N'İnternet Reklamı', 1)
INSERT [dbo].[T_PARAMS] ([id], [grup], [keys], [value], [sira_no]) VALUES (5, N'bizi_nereden_duydunuz', N'2', N'Arkadaş Tavsiyesi', 2)
INSERT [dbo].[T_PARAMS] ([id], [grup], [keys], [value], [sira_no]) VALUES (6, N'bizi_nereden_duydunuz', N'3', N'Diğer', 3)
INSERT [dbo].[T_PARAMS] ([id], [grup], [keys], [value], [sira_no]) VALUES (7, N'kredi_limit_tavsiye_eden', N'kredi_limit_tavsiye_eden', N'50', 0)
INSERT [dbo].[T_PARAMS] ([id], [grup], [keys], [value], [sira_no]) VALUES (8, N'kredi_limit_tavsiye_edilen', N'kredi_limit_tavsiye_edilen', N'25', 0)
INSERT [dbo].[T_PARAMS] ([id], [grup], [keys], [value], [sira_no]) VALUES (9, N'kredi_limit_blog', N'kredi_limit_blog', N'50', 0)
INSERT [dbo].[T_PARAMS] ([id], [grup], [keys], [value], [sira_no]) VALUES (10, N'kredi_limit_makale', N'kredi_limit_makale', N'50', 0)
INSERT [dbo].[T_PARAMS] ([id], [grup], [keys], [value], [sira_no]) VALUES (11, N'avukat_aktiflik_blog_puan', N'avukat_aktiflik_blog_puan', N'10', 0)
INSERT [dbo].[T_PARAMS] ([id], [grup], [keys], [value], [sira_no]) VALUES (12, N'avukat_aktiflik_makale_puan', N'avukat_aktiflik_makale_puan', N'10', 0)
INSERT [dbo].[T_PARAMS] ([id], [grup], [keys], [value], [sira_no]) VALUES (13, N'avukat_aktiflik_tavsiye_et', N'avukat_aktiflik_tavsiye_et', N'5', 0)
INSERT [dbo].[T_PARAMS] ([id], [grup], [keys], [value], [sira_no]) VALUES (14, N'avukat_sikayet_okuma', N'avukat_sikayet_okuma', N'1', 0)
INSERT [dbo].[T_PARAMS] ([id], [grup], [keys], [value], [sira_no]) VALUES (15, N'avukat_sikayet_teklif_ver', N'0', N'15', 0)
INSERT [dbo].[T_PARAMS] ([id], [grup], [keys], [value], [sira_no]) VALUES (16, N'avukat_sikayet_teklif_ver', N'1', N'10', 0)
INSERT [dbo].[T_PARAMS] ([id], [grup], [keys], [value], [sira_no]) VALUES (17, N'avukat_sikayet_teklif_ver', N'2', N'5', 0)
INSERT [dbo].[T_PARAMS] ([id], [grup], [keys], [value], [sira_no]) VALUES (18, N'avukat_sehir_secim', N'avukat_sehir_secim', N'3', 0)
INSERT [dbo].[T_PARAMS] ([id], [grup], [keys], [value], [sira_no]) VALUES (19, N'avukat_brans_secim', N'avukat_brans_secim', N'5', 0)
INSERT [dbo].[T_PARAMS] ([id], [grup], [keys], [value], [sira_no]) VALUES (20, N'sikayet_durum', N'95', N'Silme Onayı', 0)
INSERT [dbo].[T_PARAMS] ([id], [grup], [keys], [value], [sira_no]) VALUES (21, N'sikayet_durum', N'90', N'Kapatıldı', 0)
INSERT [dbo].[T_PARAMS] ([id], [grup], [keys], [value], [sira_no]) VALUES (22, N'sikayet_durum', N'91', N'Silindi', 0)
INSERT [dbo].[T_PARAMS] ([id], [grup], [keys], [value], [sira_no]) VALUES (23, N'dosya_durum', N'1', N'okuma', 0)
INSERT [dbo].[T_PARAMS] ([id], [grup], [keys], [value], [sira_no]) VALUES (24, N'dosya_durum', N'5', N'teklif verme', 0)
INSERT [dbo].[T_PARAMS] ([id], [grup], [keys], [value], [sira_no]) VALUES (25, N'avukat_sikayet_teklif_bedava', N'avukat_sikayet_teklif_bedava', N'3', 0)
INSERT [dbo].[T_PARAMS] ([id], [grup], [keys], [value], [sira_no]) VALUES (26, N'avukat_sehir_ekleme_kredi', N'avukat_sehir_ekleme_kredi', N'3', 0)
INSERT [dbo].[T_PARAMS] ([id], [grup], [keys], [value], [sira_no]) VALUES (27, N'avukat_brans_ekleme_kredi', N'avukat_brans_ekleme_kredi', N'3', 0)
INSERT [dbo].[T_PARAMS] ([id], [grup], [keys], [value], [sira_no]) VALUES (28, N'avukat_ana_sayfaya_ekleme_kredi', N'avukat_ana_sayfaya_ekleme_kredi', N'5', 0)
INSERT [dbo].[T_PARAMS] ([id], [grup], [keys], [value], [sira_no]) VALUES (29, N'avukat_profil_anasayfada_kalma_suresi', N'avukat_profil_anasayfada_kalma_suresi', N'7', 0)
SET IDENTITY_INSERT [dbo].[T_PARAMS] OFF
SET IDENTITY_INSERT [dbo].[T_SEHIR] ON 

INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (1, 1, N'Adana', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (2, 2, N'Adıyaman', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (3, 3, N'Afyon', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (4, 4, N'Ağrı', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (5, 5, N'Amasya', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (6, 6, N'Ankara', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (7, 7, N'Antalya', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (8, 8, N'Artvin', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (9, 9, N'Aydın', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (10, 10, N'Balıkesir', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (11, 11, N'Bilecik', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (12, 12, N'Bingöl', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (13, 13, N'Bitlis', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (14, 14, N'Bolu', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (15, 15, N'Burdur', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (16, 16, N'Bursa', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (17, 17, N'Çanakkale', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (18, 18, N'Çankırı', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (19, 19, N'Çorum', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (20, 20, N'Denizli', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (21, 21, N'Diyarbakır', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (22, 22, N'Edirne', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (23, 23, N'Elazığ', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (24, 24, N'Erzincan', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (25, 25, N'Erzurum', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (26, 26, N'Eskişehir', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (27, 27, N'Gaziantep', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (28, 28, N'Giresun', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (29, 29, N'Gümüşhane', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (30, 30, N'Hakkari', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (31, 31, N'Hatay', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (32, 32, N'Isparta', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (33, 33, N'İçel', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (34, 34, N'İstanbul', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (35, 35, N'İzmir', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (36, 36, N'Kars', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (37, 37, N'Kastamonu', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (38, 38, N'Kayseri', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (39, 39, N'Kırklareli', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (40, 40, N'Kırşehir', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (41, 41, N'Kocaeli', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (42, 42, N'Konya', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (43, 43, N'Kütahya', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (44, 44, N'Malatya', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (45, 45, N'Manisa', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (46, 46, N'Kahramanmaraş', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (47, 47, N'Mardin', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (48, 48, N'Muğla', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (49, 49, N'Muş', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (50, 50, N'Nevşehir', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (51, 51, N'Niğde', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (52, 52, N'Ordu', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (53, 53, N'Rize', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (54, 54, N'Sakarya', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (55, 55, N'Samsun', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (56, 56, N'Siirt', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (57, 57, N'Sinop', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (58, 58, N'Sivas', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (59, 59, N'Tekirdağ', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (60, 60, N'Tokat', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (61, 61, N'Trabzon', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (62, 62, N'Tunceli', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (63, 63, N'Şanlıurfa', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (64, 64, N'Uşak', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (65, 65, N'Van', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (66, 66, N'Yozgat', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (67, 67, N'Zonguldak', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (68, 68, N'Aksaray', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (69, 69, N'Bayburt', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (70, 70, N'Karaman', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (71, 71, N'Kırıkkale', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (72, 72, N'Batman', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (73, 73, N'Şırnak', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (74, 74, N'Bartın', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (75, 75, N'Ardahan', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (76, 76, N'Iğdır', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (77, 77, N'Yalova', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (78, 78, N'Karabük', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (79, 79, N'Kilis', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (80, 80, N'Osmaniye', 0)
INSERT [dbo].[T_SEHIR] ([id], [il_id], [il_adi], [il_oncelik]) VALUES (81, 81, N'Düzce', 0)
SET IDENTITY_INSERT [dbo].[T_SEHIR] OFF
SET IDENTITY_INSERT [dbo].[T_SIKAYET] ON 

INSERT [dbo].[T_SIKAYET] ([id], [dosya_kod], [adi_soyadi], [cep_telefonu], [e_posta_adresi], [il_id], [brans_id], [iletisim_tercihi], [sikayet_aciklama], [dosya1], [dosya2], [dosya3], [dosya_kapat], [dosya_kapat_neden], [sil_kodu], [sil_hata_sayisi], [islem_tarihi], [durumu], [sikayet_tarihi]) VALUES (7, N'XJ667647196', N'İbrahim Maden', N'5325566699', N'oguzhan1981@gmail.com', 34, 2, 2, N'aasdasd asdas asdads', NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, 0, CAST(N'2020-12-12 18:48:00' AS SmallDateTime))
INSERT [dbo].[T_SIKAYET] ([id], [dosya_kod], [adi_soyadi], [cep_telefonu], [e_posta_adresi], [il_id], [brans_id], [iletisim_tercihi], [sikayet_aciklama], [dosya1], [dosya2], [dosya3], [dosya_kapat], [dosya_kapat_neden], [sil_kodu], [sil_hata_sayisi], [islem_tarihi], [durumu], [sikayet_tarihi]) VALUES (8, N'HO122439662', N'Mevlüt Temiz', N'5333468985', N'oguzhan1981@gmail.com', 34, 2, 2, N'
<p>Merhaba</p>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla hendrerit nisl venenatis enim tempus, sit amet mollis est semper. Cras porta sollicitudin lacinia. Aliquam quis odio at nulla bibendum pharetra vel nec erat. Morbi gravida consequat metus efficitur bibendum. Maecenas interdum velit semper, fermentum risus eu, aliquam enim. Donec quis ultricies orci. Mauris dictum, orci posuere iaculis tempus, purus sem varius nunc, quis pretium odio augue vitae erat. Aenean eu mauris at odio pellentesque congue et bibendum tellus. Duis sit amet purus cursus, sodales mi quis, cursus velit. Phasellus vitae lacus at odio pretium porta. Pellentesque ullamcorper est rutrum libero lobortis fringilla. Nullam ullamcorper porttitor lectus.</p>
<p><strong>Fusce nec finibus mauris, mattis sollicitudin diam. Suspendisse malesuada sagittis luctus. Sed eu tempor sem, ac consequat velit. Maecenas at convallis nunc. Duis nulla elit, porta vitae hendrerit sit amet, pulvinar et magna. Suspendisse sit amet volutpat ligula. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Mauris volutpat mi et elementum malesuada. Mauris id convallis lacus. Sed maximus sagittis risus, nec aliquet quam dapibus ut. Cras auctor pharetra nibh sit amet pharetra. Etiam posuere aliquam augue vulputate convallis. Sed nec scelerisque orci.</strong></p>
<p>Suspendisse placerat eleifend eros, id blandit mi placerat in. Morbi pellentesque urna id molestie varius. Morbi posuere semper est, nec sollicitudin lorem
<strong>ultricies </strong>nec. Fusce finibus blandit purus, eget vulputate mauris consequat vel. Cras at orci mollis, dignissim dolor et, hendrerit lacus. Suspendisse dignissim non justo eu tempor. Proin porttitor orci eu pharetra facilisis. Maecenas suscipit elit ut interdum pharetra.</p>
<p>Vivamus vitae ornare ligula. Sed non ligula euismod justo hendrerit aliquam. Pellentesque ultrices lorem a nulla laoreet rutrum. Donec pellentesque libero vel tempus accumsan. Donec suscipit eleifend sapien vitae porta. Phasellus at mauris blandit, elementum augue ut, porta ligula. Nullam ac venenatis odio, at tristique est. Aliquam interdum massa nec ullamcorper vulputate. Sed ipsum magna, sagittis eget justo at, malesuada interdum quam. Nunc non congue tellus. Fusce vitae aliquet quam. Pellentesque blandit ex id fringilla rhoncus.</p>
<p>Nullam rutrum elit ipsum, at fermentum nunc condimentum sed. Quisque sit amet nibh sit amet purus dignissim sagittis vel vel tellus. In tincidunt tellus turpis, ac pulvinar mi lobortis nec. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Cras varius, enim sit amet tempor rhoncus, nibh ipsum fermentum justo, non elementum sem ante non enim. Integer vel pulvinar eros, nec consectetur lorem. Donec cursus elit eget lacus vestibulum, ut accumsan ex posuere.</p>
<p>Nulla velit magna, rutrum at risus vel, tincidunt lacinia nisi. Nulla tristique augue at euismod feugiat. Maecenas iaculis placerat nibh eu luctus. Duis vel arcu nulla. Sed molestie ultrices ex, sed efficitur felis. Mauris euismod finibus tellus, non sodales magna mattis ac. Quisque id neque tempor, maximus nulla at, posuere lacus. Nulla ac neque et nibh vulputate vestibulum at maximus orci. Etiam placerat nisl velit, vel faucibus nulla viverra sagittis.</p>
<p>Mauris at quam dapibus, consequat enim ut, commodo velit. Vestibulum auctor lobortis dolor, a pretium erat accumsan eu. Aliquam vitae erat vel nibh eleifend mollis in non velit. In interdum nunc eget nisi pulvinar, quis pulvinar ex luctus. Phasellus velit lectus, imperdiet a sagittis at, imperdiet et dui. Proin pellentesque fringilla ultrices. Nulla pretium felis et dolor semper lobortis. Nunc suscipit nisl sit amet arcu mattis, at tempus dui dignissim. Quisque scelerisque pretium lacus et commodo. Sed sit amet nunc dictum tellus ultricies tempus. Maecenas id nulla quis neque auctor ornare eget nec tellus. Aliquam tristique orci nulla, eu maximus lorem tempus a.</p>
<p>Mauris felis purus, malesuada hendrerit orci sit amet, volutpat ornare ante. Aenean id fermentum orci, vitae semper libero. Praesent ut ipsum varius, blandit nisl quis, tristique ante. Suspendisse fermentum, nisi a luctus auctor, diam leo tristique ipsum, eget ultrices tortor lectus vel purus. Donec non urna turpis. Pellentesque imperdiet ipsum eget tortor dignissim, vel congue ligula sagittis. Sed rhoncus leo sed faucibus faucibus.</p>
<p>Mauris eu nisi vulputate purus euismod scelerisque eu ac diam. Nam vel ligula in ex blandit congue in non metus. Vivamus tempus non lectus eget sollicitudin. Pellentesque iaculis ante at tincidunt ullamcorper. Ut ut tristique risus. Duis lorem odio, auctor vitae rutrum id, rutrum a mi. Etiam ac ultrices lectus, sit amet congue ante. Etiam rhoncus ullamcorper eros ut feugiat. Integer ac tempor enim, ut eleifend quam.</p>
<p>Phasellus viverra porta dictum. Phasellus id porttitor neque. Sed eget sapien ac nunc tempor semper blandit ac metus. Proin et felis varius, pharetra urna consequat, rhoncus libero. Vestibulum quis porta arcu, a elementum metus. Etiam viverra viverra nulla, at finibus mauris fringilla quis. Morbi id suscipit massa. Aenean et faucibus ligula. Maecenas vestibulum ligula quis neque porta, quis commodo mi imperdiet. Praesent mattis urna id lacus auctor congue. Cras ut mi nec quam viverra rutrum sed eu turpis. Phasellus tristique sem et ultrices egestas. Curabitur eu porttitor nulla, vel aliquam erat. Aenean luctus luctus ex sit amet sollicitudin. Morbi lectus dui, pharetra vulputate sem ut, porta rutrum nibh.</p>
<p>İyi günler.</p>
', NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, 0, CAST(N'2020-12-19 16:29:00' AS SmallDateTime))
INSERT [dbo].[T_SIKAYET] ([id], [dosya_kod], [adi_soyadi], [cep_telefonu], [e_posta_adresi], [il_id], [brans_id], [iletisim_tercihi], [sikayet_aciklama], [dosya1], [dosya2], [dosya3], [dosya_kapat], [dosya_kapat_neden], [sil_kodu], [sil_hata_sayisi], [islem_tarihi], [durumu], [sikayet_tarihi]) VALUES (9, N'CB334121793', N'Mevlüt Temiz', N'5333468985', N'oguzhan1981@gmail.com', 34, 2, 2, N'
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla hendrerit nisl venenatis enim tempus, sit amet mollis est semper. Cras porta sollicitudin lacinia. Aliquam quis odio at nulla bibendum pharetra vel nec erat. Morbi gravida consequat metus efficitur bibendum. Maecenas interdum velit semper, fermentum risus eu, aliquam enim. Donec quis ultricies orci. Mauris dictum, orci posuere iaculis tempus, purus sem varius nunc, quis pretium odio augue vitae erat. Aenean eu mauris at odio pellentesque congue et bibendum tellus. Duis sit amet purus cursus, sodales mi quis, cursus velit. Phasellus vitae lacus at odio pretium porta. Pellentesque ullamcorper est rutrum libero lobortis fringilla. Nullam ullamcorper porttitor lectus.</p>
<p>Fusce nec finibus mauris, mattis sollicitudin diam. Suspendisse malesuada sagittis luctus. Sed eu tempor sem, ac consequat velit. Maecenas at convallis nunc. Duis nulla elit, porta vitae hendrerit sit amet, pulvinar et magna. Suspendisse sit amet volutpat ligula. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Mauris volutpat mi et elementum malesuada. Mauris id convallis lacus. Sed maximus sagittis risus, nec aliquet quam dapibus ut. Cras auctor pharetra nibh sit amet pharetra. Etiam posuere aliquam augue vulputate convallis. Sed nec scelerisque orci.</p>
<p>Suspendisse placerat eleifend eros, id blandit mi placerat in. Morbi pellentesque urna id molestie varius. Morbi posuere semper est, nec sollicitudin lorem ultricies nec. Fusce finibus blandit purus, eget vulputate mauris consequat vel. Cras at orci mollis, dignissim dolor et, hendrerit lacus. Suspendisse dignissim non justo eu tempor. Proin porttitor orci eu pharetra facilisis. Maecenas suscipit elit ut interdum pharetra.</p>
<p>Vivamus vitae ornare ligula. Sed non ligula euismod justo hendrerit aliquam. Pellentesque ultrices lorem a nulla laoreet rutrum. Donec pellentesque libero vel tempus accumsan. Donec suscipit eleifend sapien vitae porta. Phasellus at mauris blandit, elementum augue ut, porta ligula. Nullam ac venenatis odio, at tristique est. Aliquam interdum massa nec ullamcorper vulputate. Sed ipsum magna, sagittis eget justo at, malesuada interdum quam. Nunc non congue tellus. Fusce vitae aliquet quam. Pellentesque blandit ex id fringilla rhoncus.</p>
<p>Nullam rutrum elit ipsum, at fermentum nunc condimentum sed. Quisque sit amet nibh sit amet purus dignissim sagittis vel vel tellus. In tincidunt tellus turpis, ac pulvinar mi lobortis nec. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Cras varius, enim sit amet tempor rhoncus, nibh ipsum fermentum justo, non elementum sem ante non enim. Integer vel pulvinar eros, nec consectetur lorem. Donec cursus elit eget lacus vestibulum, ut accumsan ex posuere.</p>
<p>Nulla velit magna, rutrum at risus vel, tincidunt lacinia nisi. Nulla tristique augue at euismod feugiat. Maecenas iaculis placerat nibh eu luctus. Duis vel arcu nulla. Sed molestie ultrices ex, sed efficitur felis. Mauris euismod finibus tellus, non sodales magna mattis ac. Quisque id neque tempor, maximus nulla at, posuere lacus. Nulla ac neque et nibh vulputate vestibulum at maximus orci. Etiam placerat nisl velit, vel faucibus nulla viverra sagittis.</p>
<p>Mauris at quam dapibus, consequat enim ut, commodo velit. Vestibulum auctor lobortis dolor, a pretium erat accumsan eu. Aliquam vitae erat vel nibh eleifend mollis in non velit. In interdum nunc eget nisi pulvinar, quis pulvinar ex luctus. Phasellus velit lectus, imperdiet a sagittis at, imperdiet et dui. Proin pellentesque fringilla ultrices. Nulla pretium felis et dolor semper lobortis. Nunc suscipit nisl sit amet arcu mattis, at tempus dui dignissim. Quisque scelerisque pretium lacus et commodo. Sed sit amet nunc dictum tellus ultricies tempus. Maecenas id nulla quis neque auctor ornare eget nec tellus. Aliquam tristique orci nulla, eu maximus lorem tempus a.</p>
<p>Mauris felis purus, malesuada hendrerit orci sit amet, volutpat ornare ante. Aenean id fermentum orci, vitae semper libero. Praesent ut ipsum varius, blandit nisl quis, tristique ante. Suspendisse fermentum, nisi a luctus auctor, diam leo tristique ipsum, eget ultrices tortor lectus vel purus. Donec non urna turpis. Pellentesque imperdiet ipsum eget tortor dignissim, vel congue ligula sagittis. Sed rhoncus leo sed faucibus faucibus.</p>
<p>Mauris eu nisi vulputate purus euismod scelerisque eu ac diam. Nam vel ligula in ex blandit congue in non metus. Vivamus tempus non lectus eget sollicitudin. Pellentesque iaculis ante at tincidunt ullamcorper. Ut ut tristique risus. Duis lorem odio, auctor vitae rutrum id, rutrum a mi. Etiam ac ultrices lectus, sit amet congue ante. Etiam rhoncus ullamcorper eros ut feugiat. Integer ac tempor enim, ut eleifend quam.</p>
<p>Phasellus viverra porta dictum. Phasellus id porttitor neque. Sed eget sapien ac nunc tempor semper blandit ac metus. Proin et felis varius, pharetra urna consequat, rhoncus libero. Vestibulum quis porta arcu, a elementum metus. Etiam viverra viverra nulla, at finibus mauris fringilla quis. Morbi id suscipit massa. Aenean et faucibus ligula. Maecenas vestibulum ligula quis neque porta, quis commodo mi imperdiet. Praesent mattis urna id lacus auctor congue. Cras ut mi nec quam viverra rutrum sed eu turpis. Phasellus tristique sem et ultrices egestas. Curabitur eu porttitor nulla, vel aliquam erat. Aenean luctus luctus ex sit amet sollicitudin. Morbi lectus dui, pharetra vulputate sem ut, porta rutrum nibh.</p>
', NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, 0, CAST(N'2020-12-19 16:32:00' AS SmallDateTime))
INSERT [dbo].[T_SIKAYET] ([id], [dosya_kod], [adi_soyadi], [cep_telefonu], [e_posta_adresi], [il_id], [brans_id], [iletisim_tercihi], [sikayet_aciklama], [dosya1], [dosya2], [dosya3], [dosya_kapat], [dosya_kapat_neden], [sil_kodu], [sil_hata_sayisi], [islem_tarihi], [durumu], [sikayet_tarihi]) VALUES (10, N'CS558189330', N'Mevlüt Temiz', N'5333468985', N'oguzhan1981@gmail.com', 34, 2, 2, N'
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc elementum ac ex vitae volutpat. Sed dignissim magna justo. Ut pretium ligula et est placerat dignissim. Nam quis auctor nisi. Mauris eu lacus quis urna elementum laoreet vel et tellus. Vestibulum elementum vulputate felis, nec fringilla nunc accumsan efficitur. Vestibulum hendrerit fermentum lacus. Suspendisse sit amet nulla nunc.</p>
<p>Nulla facilisi. Donec id dolor vitae sapien laoreet rhoncus id sit amet sapien. Nullam sit amet augue eu ante tempus sodales quis at risus. Morbi eget velit nulla. Nullam cursus nisl eu dolor iaculis facilisis. Pellentesque placerat ligula neque, vestibulum ornare arcu semper in. Nulla augue tellus, bibendum quis sodales rhoncus, sodales id ligula. Suspendisse suscipit auctor scelerisque. Sed in lobortis ipsum, at elementum sem. Interdum et malesuada fames ac ante ipsum primis in faucibus. Etiam ullamcorper lorem in enim blandit, nec mattis ante rhoncus. Aliquam pulvinar rhoncus sem eget facilisis. Suspendisse non condimentum ex, egestas aliquet elit.</p>
<p>Aliquam egestas efficitur nisi, in aliquam risus fermentum eu. Pellentesque blandit arcu vitae leo finibus, sit amet tempus orci consectetur. In hac habitasse platea dictumst. Maecenas blandit nulla ligula, quis tincidunt urna molestie nec. Nam porttitor enim magna, quis laoreet urna egestas sed. Fusce ullamcorper blandit gravida. Maecenas malesuada massa libero. Mauris maximus mi ligula, et porta nulla tincidunt et.</p>
<ol>
<li>Interdum et malesuada fames ac ante ipsum primis in faucibus. Vivamus porttitor lectus nisi, sed viverra ex luctus tincidunt. Nulla mattis purus ac laoreet molestie.&nbsp;</li><li>Nam dictum dictum nibh vitae faucibus. Nam convallis condimentum gravida. Nulla varius pellentesque massa, at laoreet odio convallis ut. Quisque ornare felis ac turpis imperdiet, ac ornare felis tristique. Fusce vitae ullamcorper nisl, in ultricies ipsum.</li></ol>
<p>Aliquam a mi nec velit tincidunt pharetra interdum id libero. In sed sem et turpis lacinia ullamcorper non in libero. Morbi at ante sit amet elit feugiat tincidunt eget ut tortor. Donec posuere rutrum finibus. Pellentesque non rutrum augue, eget imperdiet lacus. Nullam ut dapibus justo, vitae luctus nulla. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin augue velit, molestie quis placerat et, mattis vitae quam. Nulla ullamcorper eros odio, at pellentesque sem egestas vitae. Fusce at massa vel dui gravida suscipit. Etiam sagittis mauris nec finibus rutrum. Sed accumsan id nulla non ornare.</p>
<p>Donec condimentum finibus nisi, eget tempor enim posuere et. Vestibulum vestibulum, ante vitae vulputate ultrices, mi dui faucibus dui, vel fermentum felis urna quis magna. Vestibulum ut urna nec mi pretium molestie eget ut felis. Sed urna metus, mollis eu metus ac, bibendum laoreet mauris. Suspendisse id vestibulum neque. Vivamus et ligula a augue eleifend lacinia id non lacus. Donec convallis imperdiet velit. Nam felis ex, pharetra non sem vel, pharetra fermentum purus. Maecenas nisi mauris, suscipit non luctus id, rhoncus ac lacus. Sed vel congue libero. Mauris vel est erat. Nulla facilisi. Nulla ante urna, rutrum a ante eu, porta volutpat diam.</p>
<p><strong>Nulla facilisi. Mauris posuere massa a massa viverra scelerisque ut vitae odio. Aliquam dictum dui quis mauris vehicula maximus. Sed convallis ut libero sed maximus. Fusce non diam ac orci eleifend pellentesque sed nec erat. Donec sit amet pharetra arcu. Aenean viverra ut orci id dictum. Morbi molestie et urna eget faucibus. Duis a efficitur enim, nec tincidunt sapien.</strong></p>
<p><strong>Morbi posuere blandit dapibus. Cras tristique eu risus finibus maximus. Sed viverra nec eros id finibus. Nam vulputate rutrum neque quis cursus. In dignissim sem dui, ut laoreet neque laoreet vitae. Aliquam eu pulvinar nisi, at euismod lorem. Cras pulvinar nunc tincidunt turpis volutpat imperdiet. Etiam sed tincidunt nulla. Sed arcu ligula, eleifend sit amet turpis at, pharetra pretium nisl.</strong></p>
<p>Nulla tristique facilisis rhoncus. Aliquam pulvinar mi erat, non pharetra arcu commodo tincidunt. Fusce eget ultricies ex. Phasellus ornare eu erat quis pellentesque. Praesent volutpat arcu eget dui auctor, sed ullamcorper metus pulvinar. Aliquam maximus est id turpis volutpat, nec pellentesque magna euismod. Donec dignissim rutrum quam, eu convallis mauris dignissim et. Nulla tempor pulvinar dapibus. Praesent tempor, ante in volutpat mollis, nisl eros cursus ex, eu condimentum lacus diam id mi. Nunc lacinia nulla et purus auctor, vitae lobortis nisl semper. Praesent tempor, nunc vestibulum aliquam dapibus, sem arcu efficitur metus, non tempus quam tortor vel nisi. Quisque pulvinar, augue in rhoncus sagittis, leo velit ornare turpis, et malesuada justo dolor ultrices nunc. Donec eget mollis quam. Maecenas et pharetra arcu, et tincidunt elit.</p>
<p>Aliquam erat volutpat. Integer tristique odio at euismod iaculis. Nullam sollicitudin finibus mi ullamcorper ultrices. Phasellus metus ipsum, pellentesque non nulla quis, vehicula pharetra justo. Nullam laoreet id risus et feugiat. Aenean vel euismod erat. Duis efficitur felis dictum orci mollis, eget sollicitudin dui rutrum. Cras tristique turpis egestas orci placerat, et pharetra orci posuere. Morbi cursus euismod sem, eget tempus leo.</p>
<p>Fusce luctus luctus ante, ut semper odio. Praesent felis nisi, malesuada eu malesuada placerat, aliquet a tortor. Donec eros nisl, commodo id convallis nec, consequat vitae enim. Integer eget dui elementum, dictum eros in, malesuada erat. Morbi vitae ex ullamcorper, congue metus nec, vulputate nisi. Nullam rhoncus, ex consectetur venenatis pharetra, orci leo suscipit augue, eu porta justo diam ut ligula. Suspendisse sit amet sapien diam. Curabitur venenatis consectetur augue malesuada vehicula. Cras suscipit at leo et hendrerit.</p>
<p>Etiam id elit eu lectus faucibus fringilla a non risus. Cras placerat, lacus vel convallis mattis, sapien felis congue dui, eu dapibus metus lectus in velit. Nulla vel ornare massa, non molestie lorem. Proin convallis sed odio id ullamcorper. Donec varius metus egestas lacinia molestie. Maecenas euismod est quam, at tristique sapien aliquam et. Aliquam erat volutpat. Pellentesque tincidunt fringilla posuere. Vestibulum fermentum et enim sit amet porta. Suspendisse quam magna, porta et lectus sit amet, posuere consectetur urna. Aliquam consequat quam nulla, eget semper neque blandit sit amet. Phasellus cursus libero a urna gravida, blandit fermentum quam pretium. Nulla in feugiat dui, id hendrerit dolor. Suspendisse porttitor mi nec dui accumsan fringilla.</p>
<p>Maecenas pharetra mollis magna eget dictum. Suspendisse dictum libero at porttitor vulputate. Suspendisse congue tristique risus sit amet porttitor. Etiam id volutpat eros. Morbi pulvinar quam at auctor laoreet. Proin aliquam finibus mi vel semper. Donec non risus vitae justo imperdiet commodo id ut lacus. Fusce rhoncus sit amet risus vitae pharetra. Nullam sed ipsum est. Vestibulum iaculis cursus est, sed commodo tellus suscipit non. Suspendisse ullamcorper viverra pretium. Aenean at elementum urna. Nam blandit, lorem at ultricies placerat, mi purus tempor augue, in tincidunt velit quam eget tellus. Donec sodales nunc sed vulputate porta. Fusce pulvinar nunc erat, at aliquam magna aliquam nec.</p>
<p>Fusce varius suscipit risus, eget imperdiet eros hendrerit faucibus. Donec egestas quam faucibus mollis interdum. Pellentesque ac massa ac felis auctor interdum. Curabitur viverra, turpis non mattis placerat, sem enim congue nisl, ac pulvinar dolor odio sed orci. Donec ac lectus est. Quisque nec lacus libero. Phasellus vulputate augue mi, at varius ipsum malesuada at. Nunc lacinia consequat commodo. Nulla quis elit gravida, maximus mi eget, tincidunt tellus. Mauris eleifend fermentum ipsum, in ultrices nisi maximus non. Sed egestas urna lorem.</p>
<p>Praesent lacinia sodales risus eget congue. Nullam eget tempor purus. Nam eu facilisis nulla. Aliquam rutrum massa sed libero sagittis molestie. Sed pulvinar facilisis erat in lobortis. Aenean at vulputate sapien. Praesent bibendum lacus ex, at hendrerit lacus vehicula vulputate. Fusce sed enim non orci scelerisque sollicitudin quis ut metus. Ut id libero vehicula, scelerisque enim eu, tempus velit. Nullam euismod tortor ligula, quis scelerisque ipsum consectetur bibendum. Suspendisse id aliquam mi, id iaculis lorem. Aliquam erat volutpat. Mauris consectetur blandit mi, et gravida diam vestibulum sed. Nunc fringilla erat felis, id condimentum nibh interdum et. Nunc sit amet consequat leo.</p>
', NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, 0, CAST(N'2020-12-19 16:36:00' AS SmallDateTime))
INSERT [dbo].[T_SIKAYET] ([id], [dosya_kod], [adi_soyadi], [cep_telefonu], [e_posta_adresi], [il_id], [brans_id], [iletisim_tercihi], [sikayet_aciklama], [dosya1], [dosya2], [dosya3], [dosya_kapat], [dosya_kapat_neden], [sil_kodu], [sil_hata_sayisi], [islem_tarihi], [durumu], [sikayet_tarihi]) VALUES (11, N'VY795566965', N'Mevlüt Temiz', N'5333468985', N'oguzhan1981@gmail.com', 34, 2, 2, N'
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin mauris massa, rutrum et bibendum et, maximus a ipsum. Nulla facilisi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Sed id egestas felis. Mauris lobortis ullamcorper ultrices. Fusce sit amet sapien sit amet ipsum faucibus euismod. Sed massa nibh, sodales a fringilla sed, luctus nec dui. Phasellus consequat tincidunt eros, sit amet vehicula tellus sollicitudin ac. Integer lobortis faucibus metus. Nunc scelerisque aliquet nunc, sed semper quam egestas non. Duis eu quam id velit faucibus lobortis. Suspendisse laoreet, ante vitae rutrum sollicitudin, metus lacus egestas metus, sit amet efficitur nibh urna ultricies odio. Aliquam quis vehicula felis.</p>
<p>Donec quis rhoncus risus. Phasellus euismod neque ac diam blandit imperdiet. Nunc ante diam, eleifend eget risus id, porttitor sollicitudin magna. Ut fringilla faucibus tortor. Fusce quis arcu sodales, viverra erat in, euismod elit. Phasellus suscipit nisi ligula, et volutpat nulla pretium sed. Curabitur egestas ex non enim lobortis, ut ultricies odio porta.</p>
<p>Morbi gravida leo sed odio dictum ultricies. Nam ut gravida mauris. Nulla molestie accumsan odio quis dignissim. Morbi tempor eget justo sed dapibus. Nam non vestibulum libero, eu consectetur ligula. Donec efficitur velit massa, quis dapibus sem tristique eu. Fusce fermentum ante mi, in cursus turpis sagittis pulvinar. Duis commodo eu nibh at vestibulum. Etiam augue velit, finibus in ultricies vel, cursus eu sem. Quisque id urna vitae nibh feugiat porttitor. Etiam nec ultrices arcu. Donec orci lectus, faucibus posuere volutpat in, rhoncus vitae sapien.</p>
<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut quis lorem vel magna lacinia porta id et diam. Nullam vulputate nunc erat, eget porttitor sapien consectetur pulvinar. Suspendisse ultrices nec ante non porta. Suspendisse potenti. Quisque malesuada sapien pharetra aliquet vehicula. Etiam sodales tortor vel mattis fermentum. Vestibulum egestas vitae enim ut tincidunt. Proin hendrerit ornare vulputate. Sed facilisis purus augue, non bibendum arcu mattis non. Pellentesque nec lectus ac nisi vehicula cursus. Curabitur quis pellentesque lectus. Praesent convallis ex nec ipsum tristique sollicitudin eu et leo. Vivamus ut tristique enim. Curabitur at mi finibus, mollis enim vitae, facilisis libero.</p>
<p>Ut porta arcu cursus, facilisis dolor sit amet, pulvinar nulla. Donec a nisl sodales, pulvinar ligula ut, aliquet erat. Vestibulum placerat fringilla auctor. Praesent maximus et ligula ac sollicitudin. Sed eget faucibus erat. Vestibulum ac volutpat velit, at scelerisque tellus. Donec egestas condimentum est ac pharetra. Praesent egestas vitae ante nec auctor. Phasellus tincidunt quam eu nibh porttitor ullamcorper. Donec augue felis, suscipit quis placerat in, imperdiet at ex. Vestibulum porttitor dolor ac odio porta varius. Maecenas ornare, nisl at mollis malesuada, ipsum nibh tempor lacus, ut condimentum leo purus a tellus. In in sem nibh. Phasellus nisi purus, interdum nec dapibus nec, scelerisque eu dui.</p>
<p>Proin orci orci, rhoncus ac purus id, pulvinar egestas enim. Praesent mauris turpis, lacinia ac quam et, vehicula tristique sem. Vestibulum facilisis fringilla posuere. Vivamus molestie bibendum libero vel cursus. Aenean faucibus porttitor ipsum id condimentum. Vivamus feugiat diam metus, quis varius odio tristique non. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer dapibus interdum nunc, non molestie metus iaculis non. Fusce sed bibendum tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
<p>Aliquam erat volutpat. Mauris sollicitudin nibh a eros scelerisque, ac bibendum ipsum fringilla. Nunc a nibh posuere, bibendum dolor sed, hendrerit diam. Vivamus scelerisque velit eu urna tincidunt, nec rhoncus nulla tincidunt. Nam risus mi, molestie in vehicula a, ultrices eget nunc. Donec rutrum ex a justo scelerisque iaculis. Donec ullamcorper neque nisi, eu faucibus erat congue at. Quisque aliquet quam quam. Vivamus porttitor pellentesque nulla quis vulputate. Nulla vitae pulvinar sem. Cras gravida elementum purus, ut ultrices odio efficitur et. Donec non augue at purus tristique semper consectetur ut diam.</p>
<p>Proin placerat pulvinar urna quis condimentum. Praesent lobortis dignissim est, a aliquam nisi consectetur id. Phasellus risus odio, cursus nec hendrerit at, posuere eget magna. Pellentesque in facilisis sem, a convallis nisl. Aenean urna massa, vulputate at magna eu, condimentum scelerisque nulla. Pellentesque sollicitudin tristique libero eget dictum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Vivamus purus sem, vestibulum eu lorem quis, porttitor sollicitudin nisi.</p>
<p>Proin pharetra tellus libero, non cursus velit iaculis vitae. Praesent lacinia luctus pretium. Donec eu condimentum quam, sed euismod libero. Sed gravida urna pretium, finibus ipsum ut, condimentum urna. Sed ut nisl lectus. Nunc in aliquet ipsum. Nulla eu aliquam purus. Nunc neque tortor, sagittis malesuada interdum nec, molestie a justo. Donec tristique eros massa, ut semper turpis venenatis nec. Ut odio sem, bibendum sed ante et, fringilla mollis augue. Quisque scelerisque turpis egestas, malesuada augue eu, rutrum augue.</p>
<p>Morbi ipsum lacus, scelerisque quis massa in, ornare interdum nunc. Integer ac sem ultrices, aliquet nisl vitae, porttitor risus. Maecenas non felis nec sapien pulvinar consectetur nec at nibh. Etiam eget elit sit amet ex molestie consequat in et velit. Duis ornare tellus tincidunt diam aliquam, vitae tempus lacus convallis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus vitae fermentum lorem.</p>
<p>Praesent a eros a purus auctor rutrum ut et enim. Curabitur ac feugiat mi. Donec porttitor ipsum nec dictum rhoncus. Duis non neque eu leo maximus bibendum sed ac lorem. Fusce risus ex, cursus ac mauris mattis, porta cursus dui. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Phasellus blandit lacinia elit, sed faucibus mauris consequat vitae. Morbi sodales nunc id enim congue, non lacinia ligula interdum. Curabitur blandit massa bibendum eleifend ullamcorper. Nullam vulputate elementum diam eu lacinia. Duis id tincidunt orci, ut scelerisque purus. Integer a vestibulum nisl. Aenean bibendum non justo quis pellentesque. Suspendisse blandit pretium ipsum, eu iaculis felis tempus eget.</p>
<p>Fusce bibendum odio a eros sollicitudin, eget blandit leo ultrices. Vestibulum et commodo nisl, sit amet placerat massa. Donec elementum massa id turpis tincidunt, a iaculis leo pharetra. Pellentesque posuere ipsum enim, nec egestas velit facilisis at. Proin egestas fermentum ligula, id rutrum leo facilisis sed. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec vitae condimentum mauris, et fringilla quam. Aliquam ac ligula eu sem vestibulum rutrum sit amet quis tellus. Phasellus rutrum tellus laoreet, ultrices ante sed, sodales purus. Quisque gravida arcu a augue volutpat, in lacinia diam sollicitudin. Curabitur quis lorem lacinia, ultrices metus quis, dictum tortor. Etiam nisi arcu, suscipit et arcu vel, vestibulum rutrum nulla. Pellentesque lacus nibh, semper eget venenatis a, euismod quis mauris. Vestibulum eu laoreet turpis.</p>
<p>Nullam feugiat odio sed aliquam pharetra. Quisque eu nunc id lectus pulvinar tristique eget iaculis magna. Morbi semper vitae eros eu fermentum. In in turpis in augue ultrices tincidunt eu vulputate quam. Quisque dignissim porttitor velit, vel tincidunt ipsum accumsan convallis. Aliquam convallis nisi in ante congue porta. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sed dui pulvinar, ultricies urna a, varius libero. Morbi ac dignissim urna, at condimentum odio. Quisque convallis porttitor dolor at cursus. Sed ac arcu tempor diam congue finibus. Maecenas vel orci tristique, pretium nunc laoreet, faucibus nisi. Pellentesque nec leo nec neque tincidunt rhoncus ut vitae metus. Aliquam non enim ac erat gravida commodo non nec nisl. Vivamus vel congue ligula. Maecenas ac ullamcorper leo.</p>
<p>Vivamus eu pretium odio. Pellentesque at tristique sapien. Aenean consectetur turpis et tortor feugiat blandit. Etiam erat ante, molestie ac odio quis, sollicitudin porta quam. Donec pellentesque purus non aliquam facilisis. Suspendisse congue egestas velit, ut imperdiet erat consequat id. Pellentesque sed scelerisque nulla. Nullam sagittis mauris eget cursus lobortis. Sed tristique purus id lorem tempus placerat. In mollis orci auctor fringilla fermentum. Sed hendrerit vitae purus at varius.</p>
<p>Phasellus interdum ut sem eget tempor. Vestibulum sed dolor eget felis fermentum lobortis. In mattis mauris eu enim efficitur congue. Etiam sit amet sem urna. Phasellus non massa eget mauris dignissim blandit vel gravida augue. Ut ex tortor, commodo non risus ac, accumsan porttitor neque. Morbi eget viverra ante, nec vulputate ante. Maecenas massa est, auctor efficitur nibh sit amet, dapibus sodales libero. Sed volutpat mi id cursus accumsan. Phasellus eros leo, suscipit id dapibus sodales, consequat et urna.</p>
<p>Aenean gravida metus nibh, quis gravida metus egestas nec. Praesent vel dui quis dolor euismod pulvinar id ut libero. Nullam sit amet pellentesque ex. Donec posuere tempus dui a iaculis. Pellentesque ut odio ac nisi tempus sagittis. Etiam vestibulum semper neque et sollicitudin. Praesent et lacinia erat. Sed massa ipsum, lobortis eu imperdiet quis, mollis et est. Morbi pellentesque finibus sapien, sed blandit velit egestas eget. Cras vehicula, lacus non sagittis pulvinar, dolor felis pellentesque arcu, viverra feugiat diam neque ut augue. Proin eget urna in justo maximus aliquam. Curabitur sollicitudin nunc eget metus luctus fringilla. Cras tristique ligula viverra, accumsan dolor quis, suscipit enim. Nulla pharetra, neque eget vulputate gravida, nibh justo auctor erat, et sollicitudin nisi quam a est. Nunc purus ligula, ornare non ornare maximus, faucibus eu quam.</p>
<p>Integer at risus pellentesque, facilisis nibh et, tincidunt elit. Donec sed rhoncus magna, molestie consectetur metus. Donec dignissim scelerisque tortor ut ornare. Suspendisse bibendum, nibh eu pretium lobortis, eros justo congue eros, et luctus purus nibh eget lacus. Ut in pretium purus. Cras metus orci, laoreet vitae varius sit amet, varius ultricies magna. Aliquam at fermentum ante, ac tincidunt turpis. Suspendisse euismod commodo ligula, non convallis tortor bibendum hendrerit. Nam consectetur viverra ligula molestie sodales. Vestibulum feugiat, tortor a finibus suscipit, ante libero venenatis dui, ac luctus risus nunc nec augue. Nunc dignissim risus quis varius tempor.</p>
<p>Aenean sollicitudin mollis lorem sed rhoncus. Suspendisse hendrerit vulputate est nec finibus. Sed malesuada posuere diam id porta. Aliquam mattis ligula nisi, vitae vestibulum arcu bibendum ac. Phasellus faucibus purus non justo viverra volutpat. Vivamus varius massa vel bibendum commodo. In vitae luctus nunc. Phasellus ac mi a elit volutpat luctus viverra at erat. Nunc vitae elit eget sapien iaculis imperdiet. Aenean ac aliquam ipsum. Quisque venenatis eros eu arcu accumsan placerat. Sed dapibus dui sed nunc tincidunt, ut lobortis nulla tristique. Sed sit amet accumsan magna.</p>
<p>Nulla et ultrices nunc. Sed ex turpis, malesuada in eros at, dictum finibus sapien. Phasellus augue ex, dapibus ac mollis quis, dapibus sed dolor. Duis nec sapien ex. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nullam at feugiat metus. In hac habitasse platea dictumst. Sed fermentum facilisis dictum. Fusce sed sodales magna.</p>
<p>Nunc at orci molestie, pretium velit in, aliquet metus. Quisque eros tellus, fringilla eu rhoncus ac, facilisis ac justo. Pellentesque ac odio finibus, tristique sem at, rutrum elit. Sed eu nisi id nulla aliquam ornare sed id ipsum. Nulla ut nisl semper, pulvinar eros nec, varius lacus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec vel risus posuere orci vestibulum pharetra vitae dignissim diam. Integer rutrum semper velit eget aliquam. Phasellus sollicitudin ante iaculis arcu elementum elementum. Nam mattis, tortor nec ullamcorper facilisis, purus augue sollicitudin turpis, a efficitur ante dui et ipsum. Donec dapibus id mi a vehicula. Donec ultrices pharetra tellus ac porta.</p>
<p>Phasellus laoreet, neque in dictum pretium, metus orci laoreet purus, id dapibus tortor risus eget mi. Donec a ipsum hendrerit, dictum ipsum vitae, vulputate sapien. Proin posuere scelerisque turpis vitae fringilla. Ut ac pretium risus, bibendum iaculis leo. Sed iaculis purus id faucibus iaculis. Cras malesuada nisi in magna cursus posuere. Curabitur laoreet turpis eu varius venenatis. Maecenas vestibulum, ligula non tincidunt tristique, massa augue dapibus dui, sed semper massa lacus eu nibh. Ut non convallis sapien. Duis tincidunt, tellus sit amet maximus commodo, augue ante pulvinar leo, sit amet faucibus ipsum nisi id ante. Mauris non tortor mauris. Nunc congue est eget nisl condimentum blandit.</p>
<p>Nam mi quam, rutrum quis aliquam nec, tempor ut massa. Vivamus pretium sem ut lorem facilisis egestas. Suspendisse tristique velit turpis, eu condimentum odio sagittis vel. Vestibulum aliquam hendrerit placerat. Nulla scelerisque auctor leo. Nunc aliquet blandit est fermentum pulvinar. Fusce vehicula mi vel urna blandit dignissim. In tellus risus, sollicitudin id venenatis quis, condimentum et nulla. Aliquam risus felis, mollis vehicula justo sed, lacinia accumsan leo. Mauris imperdiet, enim vitae dignissim pellentesque, magna turpis dapibus sapien, ac auctor sapien odio vitae magna. Maecenas dignissim, tortor et blandit luctus, nibh enim porttitor magna, quis malesuada purus purus quis urna. Sed id nunc ut tellus aliquet lobortis a cursus augue. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed elit ipsum, auctor et dapibus et, lobortis nec ex. Donec faucibus vehicula pretium.</p>
<p>Nunc varius tempor mi, vel sagittis nulla finibus et. Quisque non erat in felis scelerisque bibendum id vel erat. Morbi et mauris eu libero vestibulum vulputate. Duis id facilisis sapien. Praesent egestas nisi eu erat hendrerit hendrerit et eu lectus. Pellentesque eu sagittis elit. Maecenas mollis quam eu lectus viverra ullamcorper. Cras elementum ultrices sapien ut facilisis. Vestibulum congue nulla et tortor pellentesque porta. Duis mollis tristique libero id fringilla. Etiam accumsan tortor vel eleifend auctor.</p>
<p>Donec ac egestas ante. Interdum et malesuada fames ac ante ipsum primis in faucibus. Cras ornare lacus justo, id placerat mi cursus a. Etiam ultrices lectus at neque sollicitudin sodales. Nulla facilisi. Suspendisse potenti. Praesent cursus blandit erat. Proin efficitur iaculis molestie. Sed quis quam in sapien porttitor venenatis at et purus. Quisque semper, magna vel interdum maximus, odio arcu congue ante, non venenatis velit turpis eget sem. Quisque mattis lacinia massa ac tincidunt. Pellentesque id nulla aliquet, molestie nisi nec, sagittis enim. Suspendisse quis massa eget urna facilisis tempus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed quis imperdiet augue. In maximus vel augue dictum pharetra.</p>
<p>Cras aliquam, nibh in posuere dignissim, quam est sollicitudin enim, at tempor lorem nibh nec risus. Etiam molestie maximus purus, ac tempus magna laoreet et. Nullam at enim accumsan libero sollicitudin fermentum. Cras id nulla non enim luctus pretium. In varius nec diam non imperdiet. Donec fermentum ex ut elementum imperdiet. In porta sed sem sed suscipit. Etiam rutrum nisl et ultrices eleifend. Donec ac nisl vulputate, ultrices nisl et, venenatis metus.</p>
', NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, 0, CAST(N'2020-12-19 16:40:00' AS SmallDateTime))
INSERT [dbo].[T_SIKAYET] ([id], [dosya_kod], [adi_soyadi], [cep_telefonu], [e_posta_adresi], [il_id], [brans_id], [iletisim_tercihi], [sikayet_aciklama], [dosya1], [dosya2], [dosya3], [dosya_kapat], [dosya_kapat_neden], [sil_kodu], [sil_hata_sayisi], [islem_tarihi], [durumu], [sikayet_tarihi]) VALUES (12, N'VV254481142', N'Oğuzhan Arslan', N'5333468985', N'oguzhan1981@gmail.com', 34, 1, 1, N'
<p>asda asdasa asdas asdasda asdasd asdasdasd asdasda sdasdasdasd sadasdaa</p>
', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-12-27 18:08:00' AS SmallDateTime))
SET IDENTITY_INSERT [dbo].[T_SIKAYET] OFF
SET IDENTITY_INSERT [dbo].[T_SIKAYET_AVUKAT] ON 

INSERT [dbo].[T_SIKAYET_AVUKAT] ([id], [sikayet_kodu], [sikayet_id], [avukat_id], [teklif_aciklama], [teklif_tarihi], [durumu]) VALUES (4, N'AN729909047', 7, 2, N'
<div>zxczxc sadasdvava as asd asd asd asdasdad asdasdasdasdad asdasda asdasdads</div>
<div>asdasd asdasdad asdasdasd asdasda dsadsadsad a</div>
<div>asd asd asdasdad <font color="#ff0000">asdasdasdad</font> asdadsad adsadasdasdadadasd aada</div>
<div><br>
</div>
<div>iyi günler dileriz.</div>
', CAST(N'2020-12-13 23:59:00' AS SmallDateTime), 5)
SET IDENTITY_INSERT [dbo].[T_SIKAYET_AVUKAT] OFF
SET IDENTITY_INSERT [dbo].[T_SLIDER_AVUKAT] ON 

INSERT [dbo].[T_SLIDER_AVUKAT] ([id], [avukat_id], [baslama_tarihi], [bitis_tarihi], [sira_no], [kayit_tarihi]) VALUES (1, 2, CAST(N'2020-12-20 00:00:00' AS SmallDateTime), CAST(N'2020-12-22 00:00:00' AS SmallDateTime), 0, CAST(N'2020-12-20 00:00:00' AS SmallDateTime))
SET IDENTITY_INSERT [dbo].[T_SLIDER_AVUKAT] OFF
ALTER TABLE [dbo].[T_AVUKAT]  WITH CHECK ADD  CONSTRAINT [FK_T_AVUKAT_T_SEHIR] FOREIGN KEY([il_id])
REFERENCES [dbo].[T_SEHIR] ([id])
GO
ALTER TABLE [dbo].[T_AVUKAT] CHECK CONSTRAINT [FK_T_AVUKAT_T_SEHIR]
GO
ALTER TABLE [dbo].[T_AVUKAT_BRANS]  WITH CHECK ADD  CONSTRAINT [FK_T_AVUKAT_BRANS_T_AVUKAT] FOREIGN KEY([avukat_id])
REFERENCES [dbo].[T_AVUKAT] ([id])
GO
ALTER TABLE [dbo].[T_AVUKAT_BRANS] CHECK CONSTRAINT [FK_T_AVUKAT_BRANS_T_AVUKAT]
GO
ALTER TABLE [dbo].[T_AVUKAT_BRANS]  WITH CHECK ADD  CONSTRAINT [FK_T_AVUKAT_BRANS_T_BRANS] FOREIGN KEY([brans_id])
REFERENCES [dbo].[T_BRANS] ([id])
GO
ALTER TABLE [dbo].[T_AVUKAT_BRANS] CHECK CONSTRAINT [FK_T_AVUKAT_BRANS_T_BRANS]
GO
ALTER TABLE [dbo].[T_AVUKAT_DOSYA_ISLEM]  WITH CHECK ADD  CONSTRAINT [FK_T_AVUKAT_DOSYA_ISLEM_T_AVUKAT] FOREIGN KEY([avukat_id])
REFERENCES [dbo].[T_AVUKAT] ([id])
GO
ALTER TABLE [dbo].[T_AVUKAT_DOSYA_ISLEM] CHECK CONSTRAINT [FK_T_AVUKAT_DOSYA_ISLEM_T_AVUKAT]
GO
ALTER TABLE [dbo].[T_AVUKAT_DOSYA_ISLEM]  WITH CHECK ADD  CONSTRAINT [FK_T_AVUKAT_DOSYA_ISLEM_T_SIKAYET] FOREIGN KEY([sikayet_id])
REFERENCES [dbo].[T_SIKAYET] ([id])
GO
ALTER TABLE [dbo].[T_AVUKAT_DOSYA_ISLEM] CHECK CONSTRAINT [FK_T_AVUKAT_DOSYA_ISLEM_T_SIKAYET]
GO
ALTER TABLE [dbo].[T_AVUKAT_KREDI_LOG]  WITH CHECK ADD  CONSTRAINT [FK_TBL_KREDI_LOG_T_AVUKAT] FOREIGN KEY([avukat_id])
REFERENCES [dbo].[T_AVUKAT] ([id])
GO
ALTER TABLE [dbo].[T_AVUKAT_KREDI_LOG] CHECK CONSTRAINT [FK_TBL_KREDI_LOG_T_AVUKAT]
GO
ALTER TABLE [dbo].[T_AVUKAT_SEHIR]  WITH CHECK ADD  CONSTRAINT [FK_T_AVUKAT_SEHIR_T_AVUKAT] FOREIGN KEY([avukat_id])
REFERENCES [dbo].[T_AVUKAT] ([id])
GO
ALTER TABLE [dbo].[T_AVUKAT_SEHIR] CHECK CONSTRAINT [FK_T_AVUKAT_SEHIR_T_AVUKAT]
GO
ALTER TABLE [dbo].[T_AVUKAT_SEHIR]  WITH CHECK ADD  CONSTRAINT [FK_T_AVUKAT_SEHIR_T_SEHIR] FOREIGN KEY([il_id])
REFERENCES [dbo].[T_SEHIR] ([id])
GO
ALTER TABLE [dbo].[T_AVUKAT_SEHIR] CHECK CONSTRAINT [FK_T_AVUKAT_SEHIR_T_SEHIR]
GO
ALTER TABLE [dbo].[T_SIKAYET]  WITH CHECK ADD  CONSTRAINT [FK_T_SIKAYET_T_BRANS] FOREIGN KEY([brans_id])
REFERENCES [dbo].[T_BRANS] ([id])
GO
ALTER TABLE [dbo].[T_SIKAYET] CHECK CONSTRAINT [FK_T_SIKAYET_T_BRANS]
GO
ALTER TABLE [dbo].[T_SIKAYET]  WITH CHECK ADD  CONSTRAINT [FK_T_SIKAYET_T_SEHIR] FOREIGN KEY([il_id])
REFERENCES [dbo].[T_SEHIR] ([id])
GO
ALTER TABLE [dbo].[T_SIKAYET] CHECK CONSTRAINT [FK_T_SIKAYET_T_SEHIR]
GO
ALTER TABLE [dbo].[T_SIKAYET_AVUKAT]  WITH CHECK ADD  CONSTRAINT [FK_T_SIKAYET_AVUKAT_T_AVUKAT] FOREIGN KEY([avukat_id])
REFERENCES [dbo].[T_AVUKAT] ([id])
GO
ALTER TABLE [dbo].[T_SIKAYET_AVUKAT] CHECK CONSTRAINT [FK_T_SIKAYET_AVUKAT_T_AVUKAT]
GO
ALTER TABLE [dbo].[T_SIKAYET_AVUKAT]  WITH CHECK ADD  CONSTRAINT [FK_T_SIKAYET_AVUKAT_T_SIKAYET] FOREIGN KEY([sikayet_id])
REFERENCES [dbo].[T_SIKAYET] ([id])
GO
ALTER TABLE [dbo].[T_SIKAYET_AVUKAT] CHECK CONSTRAINT [FK_T_SIKAYET_AVUKAT_T_SIKAYET]
GO
ALTER TABLE [dbo].[T_SLIDER_AVUKAT]  WITH CHECK ADD  CONSTRAINT [FK_T_SLIDER_AVUKAT_T_AVUKAT] FOREIGN KEY([avukat_id])
REFERENCES [dbo].[T_AVUKAT] ([id])
GO
ALTER TABLE [dbo].[T_SLIDER_AVUKAT] CHECK CONSTRAINT [FK_T_SLIDER_AVUKAT_T_AVUKAT]
GO
